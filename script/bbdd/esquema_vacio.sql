--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.visita DROP CONSTRAINT vta_zona_id_fk;
ALTER TABLE ONLY public.visita DROP CONSTRAINT vta_vte_pers_id_fk;
ALTER TABLE ONLY public.visita DROP CONSTRAINT vta_usu_id_fk;
ALTER TABLE ONLY public.visita DROP CONSTRAINT vta_tpvitante_id_fk;
ALTER TABLE ONLY public.visita DROP CONSTRAINT vta_proc_id_fk;
ALTER TABLE ONLY public.visita DROP CONSTRAINT vta_pers_id_fk;
ALTER TABLE ONLY public.visita DROP CONSTRAINT vta_motivo_id_fk;
ALTER TABLE ONLY public.visita DROP CONSTRAINT vta_cgo_vtante_fk;
ALTER TABLE ONLY public.usuario_rol DROP CONSTRAINT usuario_id_fk;
ALTER TABLE ONLY public.usuario DROP CONSTRAINT usu_per_fk;
ALTER TABLE ONLY public.usuario_rol DROP CONSTRAINT rol_id_fk;
ALTER TABLE ONLY public.personal DROP CONSTRAINT persn_zona_id_fk;
ALTER TABLE ONLY public.personal DROP CONSTRAINT persn_pers_id_fk;
ALTER TABLE ONLY public.personal DROP CONSTRAINT persn_dpt_fk;
ALTER TABLE ONLY public.personal DROP CONSTRAINT persn_cargo_fk;
ALTER TABLE ONLY public.persona DROP CONSTRAINT pers_tipo_vis_fk;
ALTER TABLE ONLY public.persona DROP CONSTRAINT pers_mun_fk;
ALTER TABLE ONLY public.persona DROP CONSTRAINT pers_ciu_fk;
ALTER TABLE ONLY public.municipio DROP CONSTRAINT estado_fk;
ALTER TABLE ONLY public.ciudad DROP CONSTRAINT estado_fk;
ALTER TABLE ONLY public.zona DROP CONSTRAINT zona_pk;
ALTER TABLE ONLY public.visita DROP CONSTRAINT visita_pk;
ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pk;
ALTER TABLE ONLY public.usuario_rol DROP CONSTRAINT usu_rol;
ALTER TABLE ONLY public.tipo_visitante DROP CONSTRAINT tipo_visitante_pk;
ALTER TABLE ONLY public.rol DROP CONSTRAINT rol_pk;
ALTER TABLE ONLY public.procedencia DROP CONSTRAINT proce_id_fk;
ALTER TABLE ONLY public.personal DROP CONSTRAINT personal_pk;
ALTER TABLE ONLY public.persona DROP CONSTRAINT persona_ci_pk;
ALTER TABLE ONLY public.municipio DROP CONSTRAINT municipio_pk;
ALTER TABLE ONLY public.motivo DROP CONSTRAINT motivo_pk;
ALTER TABLE ONLY public.estado DROP CONSTRAINT estado_pk;
ALTER TABLE ONLY public.departamento DROP CONSTRAINT dpto_pk;
ALTER TABLE ONLY public.ciudad DROP CONSTRAINT ciudad_pk;
ALTER TABLE ONLY public.cargo DROP CONSTRAINT cargo_pk;
ALTER TABLE public.zona ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.visita ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.tipo_visitante ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.procedencia ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.motivo ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.departamento ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.cargo ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.zona_id_seq;
DROP TABLE public.zona;
DROP SEQUENCE public.visita_id_seq;
DROP TABLE public.visita;
DROP TABLE public.usuario_rol;
DROP TABLE public.usuario;
DROP SEQUENCE public.tipo_visitante_id_seq;
DROP TABLE public.tipo_visitante;
DROP TABLE public.rol;
DROP SEQUENCE public.procedencia_id_seq;
DROP TABLE public.procedencia;
DROP TABLE public.personal;
DROP TABLE public.persona;
DROP TABLE public.municipio;
DROP SEQUENCE public.motivo_id_seq;
DROP TABLE public.motivo;
DROP TABLE public.estado;
DROP SEQUENCE public.departamento_id_seq;
DROP TABLE public.departamento;
DROP TABLE public.ciudad;
DROP SEQUENCE public.cargo_id_seq;
DROP TABLE public.cargo;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: grata; Type: COMMENT; Schema: -; Owner: grata
--

COMMENT ON DATABASE grata IS 'Base de Datos del Sistema Grata';


--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cargo; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE cargo (
    id integer NOT NULL,
    denominacion character varying(50) DEFAULT 'Sin denominación'::character varying NOT NULL,
    estatus character varying(3) DEFAULT 'ACT'::character varying NOT NULL,
    CONSTRAINT cargo_estatus_cc CHECK (((estatus)::text ~ '(ACT|INA)'::text))
);


ALTER TABLE public.cargo OWNER TO grata;

--
-- Name: TABLE cargo; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE cargo IS 'maestro de Cargos del Personal';


--
-- Name: COLUMN cargo.id; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN cargo.id IS 'Identificador del Cargo';


--
-- Name: COLUMN cargo.denominacion; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN cargo.denominacion IS 'Denominación del Cargo';


--
-- Name: COLUMN cargo.estatus; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN cargo.estatus IS 'Estatus del registro del Cargo
ACT=Activo
INA=Inactivo';


--
-- Name: cargo_id_seq; Type: SEQUENCE; Schema: public; Owner: grata
--

CREATE SEQUENCE cargo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cargo_id_seq OWNER TO grata;

--
-- Name: cargo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grata
--

ALTER SEQUENCE cargo_id_seq OWNED BY cargo.id;


--
-- Name: ciudad; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE ciudad (
    id_ciu integer NOT NULL,
    nombre character varying(75) NOT NULL,
    estado integer NOT NULL,
    estatus character varying(3) NOT NULL
);


ALTER TABLE public.ciudad OWNER TO grata;

--
-- Name: TABLE ciudad; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE ciudad IS 'Maestro de Ciudades';


--
-- Name: COLUMN ciudad.id_ciu; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN ciudad.id_ciu IS 'Identificador de la ciudad';


--
-- Name: COLUMN ciudad.nombre; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN ciudad.nombre IS 'Nombre de la ciudad';


--
-- Name: COLUMN ciudad.estado; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN ciudad.estado IS 'Identificador del estado al que pertenece la ciudad';


--
-- Name: COLUMN ciudad.estatus; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN ciudad.estatus IS 'Estatus del registro de la ciudad';


--
-- Name: departamento; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE departamento (
    id integer NOT NULL,
    denominacion character varying(30) NOT NULL,
    estatus character varying(3) DEFAULT 'ACT'::character varying NOT NULL,
    CONSTRAINT dpt_estatus_ck CHECK (((estatus)::text ~ 'ACT|INA'::text))
);


ALTER TABLE public.departamento OWNER TO grata;

--
-- Name: TABLE departamento; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE departamento IS 'Maestro de Departamentos';


--
-- Name: COLUMN departamento.id; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN departamento.id IS 'Identificador del Departamento';


--
-- Name: COLUMN departamento.denominacion; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN departamento.denominacion IS 'Denominación del Departamento';


--
-- Name: COLUMN departamento.estatus; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN departamento.estatus IS 'Estatus del registro del Departamento
ACT=Activo
INA=Inactivo';


--
-- Name: departamento_id_seq; Type: SEQUENCE; Schema: public; Owner: grata
--

CREATE SEQUENCE departamento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.departamento_id_seq OWNER TO grata;

--
-- Name: departamento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grata
--

ALTER SEQUENCE departamento_id_seq OWNED BY departamento.id;


--
-- Name: estado; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE estado (
    id_edo integer NOT NULL,
    nombre character varying(50) NOT NULL,
    estatus character varying(3) NOT NULL,
    abrv character varying(3) NOT NULL
);


ALTER TABLE public.estado OWNER TO grata;

--
-- Name: TABLE estado; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE estado IS 'Maestro de Estados y/o Dependencias Federales';


--
-- Name: COLUMN estado.id_edo; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN estado.id_edo IS 'Identificador del estado';


--
-- Name: COLUMN estado.nombre; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN estado.nombre IS 'Nombre del estado';


--
-- Name: COLUMN estado.estatus; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN estado.estatus IS 'Estatus del registro del estado';


--
-- Name: COLUMN estado.abrv; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN estado.abrv IS 'Abreviatura del estado';


--
-- Name: motivo; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE motivo (
    id integer NOT NULL,
    denominacion character varying(30) NOT NULL,
    estatus character varying(3) DEFAULT 'ACT'::character varying NOT NULL,
    CONSTRAINT motivo_estado_check CHECK (((estatus)::text ~ 'ACT|INA'::text))
);


ALTER TABLE public.motivo OWNER TO grata;

--
-- Name: TABLE motivo; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE motivo IS 'Maestro de Motivos de Visita';


--
-- Name: COLUMN motivo.id; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN motivo.id IS 'Identificador del Motivo';


--
-- Name: COLUMN motivo.denominacion; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN motivo.denominacion IS 'Denominación del Motivo';


--
-- Name: COLUMN motivo.estatus; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN motivo.estatus IS 'Estado del Motivo';


--
-- Name: motivo_id_seq; Type: SEQUENCE; Schema: public; Owner: grata
--

CREATE SEQUENCE motivo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.motivo_id_seq OWNER TO grata;

--
-- Name: motivo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grata
--

ALTER SEQUENCE motivo_id_seq OWNED BY motivo.id;


--
-- Name: municipio; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE municipio (
    id_mun integer NOT NULL,
    nombre character varying(50) NOT NULL,
    estatus character varying(3) NOT NULL,
    estado integer NOT NULL
);


ALTER TABLE public.municipio OWNER TO grata;

--
-- Name: TABLE municipio; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE municipio IS 'Maestro de Municipios';


--
-- Name: COLUMN municipio.id_mun; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN municipio.id_mun IS 'Identificador del municipio';


--
-- Name: COLUMN municipio.nombre; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN municipio.nombre IS 'Nombre del municipio';


--
-- Name: COLUMN municipio.estatus; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN municipio.estatus IS 'Estatus del registro del municipio';


--
-- Name: COLUMN municipio.estado; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN municipio.estado IS 'Estado al que pertenece el municipio';


--
-- Name: persona; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE persona (
    id integer NOT NULL,
    nombre character varying(20) NOT NULL,
    apellido character varying(20) NOT NULL,
    sexo "char" DEFAULT 'M'::"char" NOT NULL,
    edo_civil "char" DEFAULT 'S'::bpchar NOT NULL,
    nacionalidad "char" DEFAULT 'V'::character varying NOT NULL,
    telefono character varying(12) NOT NULL,
    correo_pers character varying(50) DEFAULT NULL::character varying,
    fecha_nacimiento date,
    direccion_hab text,
    municipio integer,
    ciudad integer,
    codigo_postal character varying(4),
    foto character varying(70),
    profesion character varying(30),
    estatus character varying(3) DEFAULT 'ACT'::character varying NOT NULL,
    tipo_visitante integer,
    CONSTRAINT pers_edo_civil_cc CHECK (((edo_civil)::text ~ '(C|D|S|V)'::text)),
    CONSTRAINT pers_estatus_cc CHECK (((estatus)::text ~ '(ACT|INA)'::text)),
    CONSTRAINT pers_nacion_cc CHECK (((nacionalidad)::text ~ '(V|E)'::text)),
    CONSTRAINT pers_sexo_cc CHECK (((sexo)::text ~ '(M|F)'::text))
);


ALTER TABLE public.persona OWNER TO grata;

--
-- Name: TABLE persona; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE persona IS 'Maestro de Personas del Sistema';


--
-- Name: COLUMN persona.id; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.id IS 'Cédula de Identidad de la persona';


--
-- Name: COLUMN persona.nombre; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.nombre IS 'Nombres de la Persona';


--
-- Name: COLUMN persona.apellido; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.apellido IS 'Apellidos de la Persona';


--
-- Name: COLUMN persona.sexo; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.sexo IS 'Género de la persona';


--
-- Name: COLUMN persona.edo_civil; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.edo_civil IS 'Estado civil : C=casado, D=divorciado, S=soltero, V=viudo';


--
-- Name: COLUMN persona.nacionalidad; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.nacionalidad IS 'Nacionalidad de la Persona';


--
-- Name: COLUMN persona.telefono; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.telefono IS 'Teléfono Personal';


--
-- Name: COLUMN persona.correo_pers; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.correo_pers IS 'Correo personal';


--
-- Name: COLUMN persona.fecha_nacimiento; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.fecha_nacimiento IS 'Fecha de Nacimiento';


--
-- Name: COLUMN persona.direccion_hab; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.direccion_hab IS 'Dirección de habitación';


--
-- Name: COLUMN persona.municipio; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.municipio IS 'Municipio de residencia de la persona';


--
-- Name: COLUMN persona.ciudad; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.ciudad IS 'Ciudad de residencia de la persona';


--
-- Name: COLUMN persona.codigo_postal; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.codigo_postal IS 'Código postal de la zona de residencia';


--
-- Name: COLUMN persona.foto; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.foto IS 'Identificador de la foto de la persona';


--
-- Name: COLUMN persona.profesion; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.profesion IS 'Profesión de la persona';


--
-- Name: COLUMN persona.estatus; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.estatus IS 'Estatus del registro de la persona';


--
-- Name: COLUMN persona.tipo_visitante; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN persona.tipo_visitante IS 'Tipo de Visitante';


--
-- Name: personal; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE personal (
    persona integer NOT NULL,
    departamento integer,
    cargo integer,
    ext_telef character varying(7),
    correo_org character varying(30),
    zona integer,
    estatus character varying(3) DEFAULT 'ACT'::character varying NOT NULL
);


ALTER TABLE public.personal OWNER TO grata;

--
-- Name: TABLE personal; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE personal IS 'Maestro de Personal';


--
-- Name: COLUMN personal.persona; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN personal.persona IS 'Identificador de la Persona';


--
-- Name: COLUMN personal.departamento; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN personal.departamento IS 'Identificador del Departamento donde labora la Persona';


--
-- Name: COLUMN personal.cargo; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN personal.cargo IS 'Identificador del Cargo que ocupa la Persona en la Organización';


--
-- Name: COLUMN personal.ext_telef; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN personal.ext_telef IS 'Número de extensión telefónica';


--
-- Name: COLUMN personal.zona; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN personal.zona IS 'Zona de permanencia de la Persona';


--
-- Name: COLUMN personal.estatus; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN personal.estatus IS 'Estado de la Persona';


--
-- Name: procedencia; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE procedencia (
    id integer NOT NULL,
    denominacion character varying(100) NOT NULL,
    id_alterno character varying(15),
    estatus character varying(3) DEFAULT 'ACT'::character varying NOT NULL,
    CONSTRAINT proc_estatus_ck CHECK (((estatus)::text ~ 'ACT|INA'::text))
);


ALTER TABLE public.procedencia OWNER TO grata;

--
-- Name: TABLE procedencia; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE procedencia IS 'Procedencia del Visitante';


--
-- Name: COLUMN procedencia.id; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN procedencia.id IS 'Identificador de la Procedencia';


--
-- Name: COLUMN procedencia.denominacion; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN procedencia.denominacion IS 'Denominación de la Procedencia';


--
-- Name: COLUMN procedencia.id_alterno; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN procedencia.id_alterno IS 'Identificador alterno de la Procedencia';


--
-- Name: COLUMN procedencia.estatus; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN procedencia.estatus IS 'Estatus del registro de la Procedencia
ACT=Activo
INA=Inactivo';


--
-- Name: procedencia_id_seq; Type: SEQUENCE; Schema: public; Owner: grata
--

CREATE SEQUENCE procedencia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.procedencia_id_seq OWNER TO grata;

--
-- Name: procedencia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grata
--

ALTER SEQUENCE procedencia_id_seq OWNED BY procedencia.id;


--
-- Name: rol; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE rol (
    id character varying(20) NOT NULL,
    denominacion character varying(30) DEFAULT 'Sin Denominación'::character varying NOT NULL,
    estatus character varying(3) DEFAULT 'ACT'::character varying NOT NULL,
    imagen text
);


ALTER TABLE public.rol OWNER TO grata;

--
-- Name: TABLE rol; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE rol IS 'Maestro de Roles del Sistema';


--
-- Name: COLUMN rol.id; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN rol.id IS 'Identificador del Rol';


--
-- Name: COLUMN rol.denominacion; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN rol.denominacion IS 'Denominación del Rol';


--
-- Name: tipo_visitante; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE tipo_visitante (
    id integer NOT NULL,
    denominacion character varying(50) NOT NULL,
    estatus character varying(3) DEFAULT 'ACT'::character varying NOT NULL,
    CONSTRAINT tvit_estatus_ck CHECK (((estatus)::text ~ '(ACT|INA)'::text))
);


ALTER TABLE public.tipo_visitante OWNER TO grata;

--
-- Name: TABLE tipo_visitante; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE tipo_visitante IS 'Maestro de Tipos de Visitante';


--
-- Name: COLUMN tipo_visitante.id; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN tipo_visitante.id IS 'Identificador del Tipo de Visitante';


--
-- Name: COLUMN tipo_visitante.denominacion; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN tipo_visitante.denominacion IS 'Denominación del Tipo de Visitante';


--
-- Name: COLUMN tipo_visitante.estatus; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN tipo_visitante.estatus IS 'Estatus del Tipo de Visitante
ACT=Activo
INA=inactivo';


--
-- Name: tipo_visitante_id_seq; Type: SEQUENCE; Schema: public; Owner: grata
--

CREATE SEQUENCE tipo_visitante_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_visitante_id_seq OWNER TO grata;

--
-- Name: tipo_visitante_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grata
--

ALTER SEQUENCE tipo_visitante_id_seq OWNED BY tipo_visitante.id;


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE usuario (
    id character varying(20) NOT NULL,
    persona integer NOT NULL,
    clave character varying(70) NOT NULL,
    estatus character varying(3) DEFAULT 'ACT'::character varying NOT NULL
);


ALTER TABLE public.usuario OWNER TO grata;

--
-- Name: TABLE usuario; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE usuario IS 'Maestro de Usuarios del Sistema';


--
-- Name: COLUMN usuario.id; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN usuario.id IS 'Identificador del usuario';


--
-- Name: COLUMN usuario.persona; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN usuario.persona IS 'Identificador de los datos personales del usuario';


--
-- Name: COLUMN usuario.clave; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN usuario.clave IS 'Clave de acceso del usuario';


--
-- Name: COLUMN usuario.estatus; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN usuario.estatus IS 'Estatus del registro del usuario';


--
-- Name: usuario_rol; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE usuario_rol (
    usuario character varying(20) NOT NULL,
    rol character varying(20) NOT NULL,
    estatus character varying(3) DEFAULT 'ACT'::character varying NOT NULL
);


ALTER TABLE public.usuario_rol OWNER TO grata;

--
-- Name: TABLE usuario_rol; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE usuario_rol IS 'Relación entre usuarios y roles.';


--
-- Name: COLUMN usuario_rol.usuario; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN usuario_rol.usuario IS 'Identificador del usuario al que se le asigna el rol';


--
-- Name: COLUMN usuario_rol.rol; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN usuario_rol.rol IS 'Identificador del rol que se le asigna al usuario';


--
-- Name: COLUMN usuario_rol.estatus; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN usuario_rol.estatus IS 'Estatus del registro';


--
-- Name: visita; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE visita (
    id integer NOT NULL,
    visitante integer NOT NULL,
    visitado integer,
    fecha_visita date DEFAULT now() NOT NULL,
    hora_visita time without time zone DEFAULT now(),
    hora_salida time without time zone,
    observaciones text,
    zona integer NOT NULL,
    estatus character varying(3) DEFAULT 'ACT'::character varying NOT NULL,
    motivo integer NOT NULL,
    procedencia integer NOT NULL,
    tipo_visitante integer NOT NULL,
    usuario character varying(20),
    cargo_vtante integer,
    nro_carnet character varying(5)
);


ALTER TABLE public.visita OWNER TO grata;

--
-- Name: TABLE visita; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE visita IS 'Registro de Visitas';


--
-- Name: COLUMN visita.id; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN visita.id IS 'Identificador de la visita';


--
-- Name: COLUMN visita.visitado; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN visita.visitado IS 'Identificador de la persona que recibe la visita';


--
-- Name: COLUMN visita.fecha_visita; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN visita.fecha_visita IS 'Dia de la visita';


--
-- Name: COLUMN visita.hora_visita; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN visita.hora_visita IS 'Hora de la visita';


--
-- Name: COLUMN visita.hora_salida; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN visita.hora_salida IS 'Hora de salida del visitante';


--
-- Name: COLUMN visita.observaciones; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN visita.observaciones IS 'Observaciones sobre la visita';


--
-- Name: COLUMN visita.zona; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN visita.zona IS 'Zona en la cual permanecerá el Visitante';


--
-- Name: COLUMN visita.estatus; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN visita.estatus IS 'Estatus de la visita';


--
-- Name: COLUMN visita.cargo_vtante; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN visita.cargo_vtante IS 'Cargo del Visitante';


--
-- Name: visita_id_seq; Type: SEQUENCE; Schema: public; Owner: grata
--

CREATE SEQUENCE visita_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.visita_id_seq OWNER TO grata;

--
-- Name: visita_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grata
--

ALTER SEQUENCE visita_id_seq OWNED BY visita.id;


--
-- Name: zona; Type: TABLE; Schema: public; Owner: grata; Tablespace: 
--

CREATE TABLE zona (
    id integer NOT NULL,
    denominacion character varying(30) NOT NULL,
    estatus character varying(3) DEFAULT 'ACT'::character varying NOT NULL,
    CONSTRAINT zona_estado_check CHECK (((estatus)::text ~ 'ACT|INA'::text))
);


ALTER TABLE public.zona OWNER TO grata;

--
-- Name: TABLE zona; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON TABLE zona IS 'Maestro de Zonas, Áreas  o Lugares permitidos para visitas';


--
-- Name: COLUMN zona.id; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN zona.id IS 'Identificador de la Zona';


--
-- Name: COLUMN zona.denominacion; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN zona.denominacion IS 'Denominación de la Zona';


--
-- Name: COLUMN zona.estatus; Type: COMMENT; Schema: public; Owner: grata
--

COMMENT ON COLUMN zona.estatus IS 'Estatus de la Zona';


--
-- Name: zona_id_seq; Type: SEQUENCE; Schema: public; Owner: grata
--

CREATE SEQUENCE zona_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.zona_id_seq OWNER TO grata;

--
-- Name: zona_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grata
--

ALTER SEQUENCE zona_id_seq OWNED BY zona.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grata
--

ALTER TABLE ONLY cargo ALTER COLUMN id SET DEFAULT nextval('cargo_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grata
--

ALTER TABLE ONLY departamento ALTER COLUMN id SET DEFAULT nextval('departamento_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grata
--

ALTER TABLE ONLY motivo ALTER COLUMN id SET DEFAULT nextval('motivo_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grata
--

ALTER TABLE ONLY procedencia ALTER COLUMN id SET DEFAULT nextval('procedencia_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grata
--

ALTER TABLE ONLY tipo_visitante ALTER COLUMN id SET DEFAULT nextval('tipo_visitante_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grata
--

ALTER TABLE ONLY visita ALTER COLUMN id SET DEFAULT nextval('visita_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grata
--

ALTER TABLE ONLY zona ALTER COLUMN id SET DEFAULT nextval('zona_id_seq'::regclass);


--
-- Name: cargo_pk; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY cargo
    ADD CONSTRAINT cargo_pk PRIMARY KEY (id);


--
-- Name: ciudad_pk; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY ciudad
    ADD CONSTRAINT ciudad_pk PRIMARY KEY (id_ciu);


--
-- Name: dpto_pk; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY departamento
    ADD CONSTRAINT dpto_pk PRIMARY KEY (id);


--
-- Name: estado_pk; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY estado
    ADD CONSTRAINT estado_pk PRIMARY KEY (id_edo);


--
-- Name: motivo_pk; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY motivo
    ADD CONSTRAINT motivo_pk PRIMARY KEY (id);


--
-- Name: municipio_pk; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY municipio
    ADD CONSTRAINT municipio_pk PRIMARY KEY (id_mun);


--
-- Name: persona_ci_pk; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT persona_ci_pk PRIMARY KEY (id);


--
-- Name: personal_pk; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY personal
    ADD CONSTRAINT personal_pk PRIMARY KEY (persona);


--
-- Name: proce_id_fk; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY procedencia
    ADD CONSTRAINT proce_id_fk PRIMARY KEY (id);


--
-- Name: rol_pk; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY rol
    ADD CONSTRAINT rol_pk PRIMARY KEY (id);


--
-- Name: tipo_visitante_pk; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY tipo_visitante
    ADD CONSTRAINT tipo_visitante_pk PRIMARY KEY (id);


--
-- Name: usu_rol; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY usuario_rol
    ADD CONSTRAINT usu_rol PRIMARY KEY (usuario, rol);


--
-- Name: usuario_pk; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pk PRIMARY KEY (id);


--
-- Name: visita_pk; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY visita
    ADD CONSTRAINT visita_pk PRIMARY KEY (id);


--
-- Name: zona_pk; Type: CONSTRAINT; Schema: public; Owner: grata; Tablespace: 
--

ALTER TABLE ONLY zona
    ADD CONSTRAINT zona_pk PRIMARY KEY (id);


--
-- Name: estado_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY ciudad
    ADD CONSTRAINT estado_fk FOREIGN KEY (estado) REFERENCES estado(id_edo);


--
-- Name: estado_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY municipio
    ADD CONSTRAINT estado_fk FOREIGN KEY (estado) REFERENCES estado(id_edo);


--
-- Name: pers_ciu_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT pers_ciu_fk FOREIGN KEY (ciudad) REFERENCES ciudad(id_ciu);


--
-- Name: pers_mun_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT pers_mun_fk FOREIGN KEY (municipio) REFERENCES municipio(id_mun);


--
-- Name: pers_tipo_vis_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT pers_tipo_vis_fk FOREIGN KEY (tipo_visitante) REFERENCES tipo_visitante(id);


--
-- Name: persn_cargo_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY personal
    ADD CONSTRAINT persn_cargo_fk FOREIGN KEY (cargo) REFERENCES cargo(id);


--
-- Name: persn_dpt_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY personal
    ADD CONSTRAINT persn_dpt_fk FOREIGN KEY (departamento) REFERENCES departamento(id);


--
-- Name: persn_pers_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY personal
    ADD CONSTRAINT persn_pers_id_fk FOREIGN KEY (persona) REFERENCES persona(id);


--
-- Name: persn_zona_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY personal
    ADD CONSTRAINT persn_zona_id_fk FOREIGN KEY (zona) REFERENCES zona(id);


--
-- Name: rol_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY usuario_rol
    ADD CONSTRAINT rol_id_fk FOREIGN KEY (rol) REFERENCES rol(id);


--
-- Name: usu_per_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usu_per_fk FOREIGN KEY (persona) REFERENCES persona(id);


--
-- Name: usuario_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY usuario_rol
    ADD CONSTRAINT usuario_id_fk FOREIGN KEY (usuario) REFERENCES usuario(id);


--
-- Name: vta_cgo_vtante_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY visita
    ADD CONSTRAINT vta_cgo_vtante_fk FOREIGN KEY (cargo_vtante) REFERENCES cargo(id);


--
-- Name: vta_motivo_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY visita
    ADD CONSTRAINT vta_motivo_id_fk FOREIGN KEY (motivo) REFERENCES motivo(id);


--
-- Name: vta_pers_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY visita
    ADD CONSTRAINT vta_pers_id_fk FOREIGN KEY (visitado) REFERENCES personal(persona) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: vta_proc_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY visita
    ADD CONSTRAINT vta_proc_id_fk FOREIGN KEY (procedencia) REFERENCES procedencia(id);


--
-- Name: vta_tpvitante_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY visita
    ADD CONSTRAINT vta_tpvitante_id_fk FOREIGN KEY (tipo_visitante) REFERENCES tipo_visitante(id);


--
-- Name: vta_usu_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY visita
    ADD CONSTRAINT vta_usu_id_fk FOREIGN KEY (usuario) REFERENCES usuario(id);


--
-- Name: vta_vte_pers_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY visita
    ADD CONSTRAINT vta_vte_pers_id_fk FOREIGN KEY (visitante) REFERENCES persona(id);


--
-- Name: vta_zona_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: grata
--

ALTER TABLE ONLY visita
    ADD CONSTRAINT vta_zona_id_fk FOREIGN KEY (zona) REFERENCES zona(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: ciudad; Type: ACL; Schema: public; Owner: grata
--

REVOKE ALL ON TABLE ciudad FROM PUBLIC;
REVOKE ALL ON TABLE ciudad FROM grata;
GRANT ALL ON TABLE ciudad TO grata;


--
-- Name: departamento; Type: ACL; Schema: public; Owner: grata
--

REVOKE ALL ON TABLE departamento FROM PUBLIC;
REVOKE ALL ON TABLE departamento FROM grata;
GRANT ALL ON TABLE departamento TO grata;


--
-- Name: estado; Type: ACL; Schema: public; Owner: grata
--

REVOKE ALL ON TABLE estado FROM PUBLIC;
REVOKE ALL ON TABLE estado FROM grata;
GRANT ALL ON TABLE estado TO grata;


--
-- Name: motivo; Type: ACL; Schema: public; Owner: grata
--

REVOKE ALL ON TABLE motivo FROM PUBLIC;
REVOKE ALL ON TABLE motivo FROM grata;
GRANT ALL ON TABLE motivo TO grata;


--
-- Name: municipio; Type: ACL; Schema: public; Owner: grata
--

REVOKE ALL ON TABLE municipio FROM PUBLIC;
REVOKE ALL ON TABLE municipio FROM grata;
GRANT ALL ON TABLE municipio TO grata;


--
-- Name: persona; Type: ACL; Schema: public; Owner: grata
--

REVOKE ALL ON TABLE persona FROM PUBLIC;
REVOKE ALL ON TABLE persona FROM grata;
GRANT ALL ON TABLE persona TO grata;


--
-- Name: personal; Type: ACL; Schema: public; Owner: grata
--

REVOKE ALL ON TABLE personal FROM PUBLIC;
REVOKE ALL ON TABLE personal FROM grata;
GRANT ALL ON TABLE personal TO grata;


--
-- Name: procedencia; Type: ACL; Schema: public; Owner: grata
--

REVOKE ALL ON TABLE procedencia FROM PUBLIC;
REVOKE ALL ON TABLE procedencia FROM grata;
GRANT ALL ON TABLE procedencia TO grata;


--
-- Name: rol; Type: ACL; Schema: public; Owner: grata
--

REVOKE ALL ON TABLE rol FROM PUBLIC;
REVOKE ALL ON TABLE rol FROM grata;
GRANT ALL ON TABLE rol TO grata;


--
-- Name: tipo_visitante; Type: ACL; Schema: public; Owner: grata
--

REVOKE ALL ON TABLE tipo_visitante FROM PUBLIC;
REVOKE ALL ON TABLE tipo_visitante FROM grata;
GRANT ALL ON TABLE tipo_visitante TO grata;


--
-- Name: usuario; Type: ACL; Schema: public; Owner: grata
--

REVOKE ALL ON TABLE usuario FROM PUBLIC;
REVOKE ALL ON TABLE usuario FROM grata;
GRANT ALL ON TABLE usuario TO grata;


--
-- Name: usuario_rol; Type: ACL; Schema: public; Owner: grata
--

REVOKE ALL ON TABLE usuario_rol FROM PUBLIC;
REVOKE ALL ON TABLE usuario_rol FROM grata;
GRANT ALL ON TABLE usuario_rol TO grata;


--
-- Name: visita; Type: ACL; Schema: public; Owner: grata
--

REVOKE ALL ON TABLE visita FROM PUBLIC;
REVOKE ALL ON TABLE visita FROM grata;
GRANT ALL ON TABLE visita TO grata;


--
-- Name: zona; Type: ACL; Schema: public; Owner: grata
--

REVOKE ALL ON TABLE zona FROM PUBLIC;
REVOKE ALL ON TABLE zona FROM grata;
GRANT ALL ON TABLE zona TO grata;


--
-- PostgreSQL database dump complete
--

