CREATE ROLE grata LOGIN PASSWORD 'grata01'
   VALID UNTIL 'infinity';
   COMMENT ON ROLE grata IS 'Usuario para bbdd Grata';

CREATE DATABASE grata
  WITH OWNER = grata
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;

COMMENT ON DATABASE grata IS 'Base de Datos del Sistema Grata';

