use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Grata';
use Grata::Controller::Departamentos;

ok( request('/departamentos')->is_success, 'Request should succeed' );
done_testing();
