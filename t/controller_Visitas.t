use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Grata';
use Grata::Controller::Visitas;

ok( request('/visitas')->is_success, 'Request should succeed' );
done_testing();
