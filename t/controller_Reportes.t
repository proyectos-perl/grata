use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'Grata' }
BEGIN { use_ok 'Grata::Controller::Reportes' }

ok( request('/reportes')->is_success, 'Request should succeed' );
done_testing();
