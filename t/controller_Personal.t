use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Grata';
use Grata::Controller::Personal;

ok( request('/personal')->is_success, 'Request should succeed' );
done_testing();
