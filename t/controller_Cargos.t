use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Grata';
use Grata::Controller::Cargos;

ok( request('/cargos')->is_success, 'Request should succeed' );
done_testing();
