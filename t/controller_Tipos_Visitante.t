use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Grata';
use Grata::Controller::Tipos_Visitante;

ok( request('/tipos_visitante')->is_success, 'Request should succeed' );
done_testing();
