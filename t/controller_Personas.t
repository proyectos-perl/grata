use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Grata';
use Grata::Controller::Personas;

ok( request('/personas')->is_success, 'Request should succeed' );
done_testing();
