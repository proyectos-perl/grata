use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Grata';
use Grata::Controller::Zonas;

ok( request('/zonas')->is_success, 'Request should succeed' );
done_testing();
