use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Grata';
use Grata::Controller::Motivos;

ok( request('/motivos')->is_success, 'Request should succeed' );
done_testing();
