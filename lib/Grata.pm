package Grata;
use Moose;
use namespace::autoclean;

use Catalyst::Runtime 5.80;

# Set flags and add plugins for the application
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a Config::General file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root
#                 directory

use Catalyst qw/
  ConfigLoader
  Static::Simple

  Authentication

  Authorization::Roles
  Authorization::ACL

  Session
  Session::Store::FastMmap
  Session::State::Cookie
  /;

extends 'Catalyst';

our $VERSION = '0.01';
$VERSION = eval $VERSION;

# Configure the application.
#
# Note that settings in grata.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

__PACKAGE__->config(

	# Disable deprecated behavior needed by old applications
	disable_component_resolution_regex_fallback => 1,
);

# Start the application
__PACKAGE__->setup();

#__PACKAGE__->allow_access_if( "/usuarios", [qw/administsrador/], );
__PACKAGE__->deny_access_unless_any( "/visitas/", [qw/administrador operador visitas/], );
__PACKAGE__->deny_access_unless_any( "/reportes/", [qw/administrador reportes/], );
__PACKAGE__->deny_access_unless_any( "/personas/", [qw/administrador personas/], );
__PACKAGE__->deny_access_unless_any( "/motivos/", [qw/administrador motivos/], );
__PACKAGE__->deny_access_unless_any( "/tipos_visitante/", [qw/administrador tipovisitante/], );
__PACKAGE__->deny_access_unless_any( "/procedencias/", [qw/administrador procedencias/], );
__PACKAGE__->deny_access_unless_any( "/zonas/", [qw/administrador zonas/], );
__PACKAGE__->deny_access_unless_any( "/departamentos/", [qw/administrador dpto/], );
__PACKAGE__->deny_access_unless_any( "/cargos/", [qw/administrador cargos/], );
__PACKAGE__->deny_access_unless_any( "/personal/", [qw/administrador personas/], );
__PACKAGE__->deny_access_unless_any( "/usuarios/", [qw/administrador usuarios/], );
__PACKAGE__->deny_access_unless_any( "/roles/", [qw/administrador roles/], );

=head1 NAME

Grata - Catalyst based application

=head1 SYNOPSIS

    script/grata_server.pl

=head1 DESCRIPTION

[enter your description here]

=head1 SEE ALSO

L<Grata::Controller::Root>, L<Catalyst>

=head1 AUTHOR

Nelo R. Tovar,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
