package Grata::Model::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2011-09-09 20:36:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:v4YF4YHASrsAeMhTIEEJcg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
