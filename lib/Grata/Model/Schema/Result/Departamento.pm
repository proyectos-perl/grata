package Grata::Model::Schema::Result::Departamento;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "EncodedColumn");

=head1 NAME

Grata::Model::Schema::Result::Departamento - Maestro de Departamentos

=cut

__PACKAGE__->table("departamento");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'departamento_id_seq'

Identificador del Departamento

=head2 denominacion

  data_type: 'varchar'
  is_nullable: 0
  size: 30

Denominación del Departamento

=head2 estatus

  data_type: 'varchar'
  default_value: 'ACT'
  is_nullable: 0
  size: 3

Estatus del registro del Departamento
ACT=Activo
INA=Inactivo

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "departamento_id_seq",
  },
  "denominacion",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "estatus",
  { data_type => "varchar", default_value => "ACT", is_nullable => 0, size => 3 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 personals

Type: has_many

Related object: L<Grata::Model::Schema::Result::Personal>

=cut

__PACKAGE__->has_many(
  "personals",
  "Grata::Model::Schema::Result::Personal",
  { "foreign.departamento" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07000 @ 2012-12-17 11:02:09
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:bqxzNwQzYV/Fo7Z2x0dNPA


# You can replace this text with custom content, and it will be preserved on regeneration
1;
