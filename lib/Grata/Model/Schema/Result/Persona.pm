package Grata::Model::Schema::Result::Persona;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "EncodedColumn");

=head1 NAME

Grata::Model::Schema::Result::Persona - Maestro de Personas del Sistema

=cut

__PACKAGE__->table("persona");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 0

Cédula de Identidad de la persona

=head2 nombre

  data_type: 'varchar'
  is_nullable: 0
  size: 20

Nombres de la Persona

=head2 apellido

  data_type: 'varchar'
  is_nullable: 0
  size: 20

Apellidos de la Persona

=head2 sexo

  data_type: '"char"'
  default_value: 'M'::"char'
  is_nullable: 0
  size: 1

Género de la persona

=head2 edo_civil

  data_type: '"char"'
  default_value: 'S'
  is_nullable: 0
  size: 1

Estado civil : C=casado, D=divorciado, S=soltero, V=viudo

=head2 nacionalidad

  data_type: '"char"'
  default_value: 'V'
  is_nullable: 0
  size: 1

Nacionalidad de la Persona

=head2 telefono

  data_type: 'varchar'
  is_nullable: 0
  size: 12

Teléfono Personal

=head2 correo_pers

  data_type: 'varchar'
  is_nullable: 0
  size: 50

Correo personal

=head2 fecha_nacimiento

  data_type: 'date'
  is_nullable: 1

Fecha de Nacimiento

=head2 direccion_hab

  data_type: 'text'
  is_nullable: 1

Dirección de habitación

=head2 municipio

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

Municipio de residencia de la persona

=head2 ciudad

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

Ciudad de residencia de la persona

=head2 codigo_postal

  data_type: 'varchar'
  is_nullable: 1
  size: 4

Código postal de la zona de residencia

=head2 foto

  data_type: 'varchar'
  is_nullable: 1
  size: 70

Identificador de la foto de la persona

=head2 profesion

  data_type: 'varchar'
  is_nullable: 1
  size: 30

Profesión de la persona

=head2 estatus

  data_type: 'varchar'
  default_value: 'ACT'
  is_nullable: 0
  size: 3

Estatus del registro de la persona

=head2 tipo_visitante

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

Tipo de Visitante

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 0 },
  "nombre",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "apellido",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "sexo",
  {
    data_type => "\"char\"",
    default_value => "M'::\"char",
    is_nullable => 0,
    size => 1,
  },
  "edo_civil",
  { data_type => "\"char\"", default_value => "S", is_nullable => 0, size => 1 },
  "nacionalidad",
  { data_type => "\"char\"", default_value => "V", is_nullable => 0, size => 1 },
  "telefono",
  { data_type => "varchar", is_nullable => 0, size => 12 },
  "correo_pers",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "fecha_nacimiento",
  { data_type => "date", is_nullable => 1 },
  "direccion_hab",
  { data_type => "text", is_nullable => 1 },
  "municipio",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "ciudad",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "codigo_postal",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "foto",
  { data_type => "varchar", is_nullable => 1, size => 70 },
  "profesion",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "estatus",
  { data_type => "varchar", default_value => "ACT", is_nullable => 0, size => 3 },
  "tipo_visitante",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 municipio

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Municipio>

=cut

__PACKAGE__->belongs_to(
  "municipio",
  "Grata::Model::Schema::Result::Municipio",
  { id_mun => "municipio" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "CASCADE",
    on_update     => "CASCADE",
  },
);

=head2 tipo_visitante

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::TipoVisitante>

=cut

__PACKAGE__->belongs_to(
  "tipo_visitante",
  "Grata::Model::Schema::Result::TipoVisitante",
  { id => "tipo_visitante" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "CASCADE",
    on_update     => "CASCADE",
  },
);

=head2 ciudad

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Ciudad>

=cut

__PACKAGE__->belongs_to(
  "ciudad",
  "Grata::Model::Schema::Result::Ciudad",
  { id_ciu => "ciudad" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "CASCADE",
    on_update     => "CASCADE",
  },
);

=head2 personal

Type: might_have

Related object: L<Grata::Model::Schema::Result::Personal>

=cut

__PACKAGE__->might_have(
  "personal",
  "Grata::Model::Schema::Result::Personal",
  { "foreign.persona" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 usuarios

Type: has_many

Related object: L<Grata::Model::Schema::Result::Usuario>

=cut

__PACKAGE__->has_many(
  "usuarios",
  "Grata::Model::Schema::Result::Usuario",
  { "foreign.persona" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 visitas

Type: has_many

Related object: L<Grata::Model::Schema::Result::Visita>

=cut

__PACKAGE__->has_many(
  "visitas",
  "Grata::Model::Schema::Result::Visita",
  { "foreign.visitante" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-16 22:18:53
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:0lyAfIv5A4ggO33JNj/73A


# You can replace this text with custom content, and it will be preserved on regeneration

sub new {
	my ( $self, $attrs ) = @_;

    if ($attrs->{nombre}){
        $attrs->{nombre}   =~ s/\A\s+//;    # Eliminar blancos al comienzo - ltrim
        $attrs->{nombre}   =~ s/\s+\z//;    # Eliminar blancos al final - rtrim
    };
    if($attrs->{apellido}){
        $attrs->{apellido} =~ s/\A\s+//;    # Eliminar blancos al comienzo - ltrim
        $attrs->{apellido} =~ s/\s+\z//;    # Eliminar blancos al final - rtrim
    }   

    my $new = $self->next::method($attrs);

	return $new;
}

sub update {
	my ( $self, $attrs ) = @_;

    if ($attrs->{nombre}){
        $attrs->{nombre}   =~ s/\A\s+//;    # Eliminar blancos al comienzo - ltrim
        $attrs->{nombre}   =~ s/\s+\z//;    # Eliminar blancos al final - rtrim
    };
    if($attrs->{apellido}){
        $attrs->{apellido} =~ s/\A\s+//;    # Eliminar blancos al comienzo - ltrim
        $attrs->{apellido} =~ s/\s+\z//;    # Eliminar blancos al final - rtrim
    };
	
    my $new = $self->next::method($attrs);

	return $new;
}

=head2 nombre_completo

Retorna el Nombre Completo de la persona

=cut

sub nombre_completo {
    my $self = shift;

    return $self->nombre . ' ' . $self->apellido;
}

=head2 texto_sexo

Retorna el sexo de la Persona

=cut

sub texto_sexo {
	my $self = shift;

	return $self->sexo eq 'M' ? 'Masculino' : 'Femenino';
}

=head2 texto_edo_civil

Retorna el estado civil de la Persona

=cut

sub texto_edo_civil {
	my $self      = shift;
	my %valores_f = (
		S => 'Soltera',
		C => 'Casada',
		D => 'Divorciada',
		V => 'Viuda'
	);
	my %valores_m = (
		S => 'Soltero',
		C => 'Casado',
		D => 'Divorciado',
		V => 'Viudo'
	);

	return $self->sexo eq 'M' ? $valores_m{$self->edo_civil} : $valores_f{$self->edo_civil};
}

=head2 texto_nacionalidad

Retorna la nacionalidad de la Persona

=cut

sub texto_nacionalidad {
	my $self = shift;

	return $self->nacionalidad eq 'V' ? 'Venezolana' : 'Extranjera';
}

1;
