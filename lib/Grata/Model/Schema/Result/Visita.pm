package Grata::Model::Schema::Result::Visita;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "EncodedColumn");

=head1 NAME

Grata::Model::Schema::Result::Visita - Registro de Visitas

=cut

__PACKAGE__->table("visita");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'visita_id_seq'

Identificador de la visita

=head2 visitante

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 visitado

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

Identificador de la persona que recibe la visita

=head2 fecha_visita

  data_type: 'date'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}

Dia de la visita

=head2 hora_visita

  data_type: 'time'
  default_value: current_timestamp
  is_nullable: 1
  original: {default_value => \"now()"}

Hora de la visita

=head2 hora_salida

  data_type: 'time'
  is_nullable: 1

Hora de salida del visitante

=head2 observaciones

  data_type: 'text'
  is_nullable: 1

Observaciones sobre la visita

=head2 zona

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

Zona en la cual permanecerá el Visitante

=head2 estatus

  data_type: 'varchar'
  default_value: 'ACT'
  is_nullable: 0
  size: 3

Estatus de la visita

=head2 motivo

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 procedencia

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 tipo_visitante

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 usuario

  data_type: 'varchar'
  is_foreign_key: 1
  is_nullable: 1
  size: 20

=head2 cargo_vtante

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

Cargo del Visitante

=head2 nro_carnet

  data_type: 'varchar'
  is_nullable: 1
  size: 5

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "visita_id_seq",
  },
  "visitante",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "visitado",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "fecha_visita",
  {
    data_type     => "date",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
  },
  "hora_visita",
  {
    data_type     => "time",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
  "hora_salida",
  { data_type => "time", is_nullable => 1 },
  "observaciones",
  { data_type => "text", is_nullable => 1 },
  "zona",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "estatus",
  { data_type => "varchar", default_value => "ACT", is_nullable => 0, size => 3 },
  "motivo",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "procedencia",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "tipo_visitante",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "usuario",
  { data_type => "varchar", is_foreign_key => 1, is_nullable => 1, size => 20 },
  "cargo_vtante",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "nro_carnet",
  { data_type => "varchar", is_nullable => 1, size => 5 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 usuario

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Usuario>

=cut

__PACKAGE__->belongs_to(
  "usuario",
  "Grata::Model::Schema::Result::Usuario",
  { id => "usuario" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "CASCADE",
    on_update     => "CASCADE",
  },
);

=head2 zona

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Zona>

=cut

__PACKAGE__->belongs_to(
  "zona",
  "Grata::Model::Schema::Result::Zona",
  { id => "zona" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 procedencia

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Procedencia>

=cut

__PACKAGE__->belongs_to(
  "procedencia",
  "Grata::Model::Schema::Result::Procedencia",
  { id => "procedencia" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 cargo_vtante

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Cargo>

=cut

__PACKAGE__->belongs_to(
  "cargo_vtante",
  "Grata::Model::Schema::Result::Cargo",
  { id => "cargo_vtante" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "CASCADE",
    on_update     => "CASCADE",
  },
);

=head2 tipo_visitante

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::TipoVisitante>

=cut

__PACKAGE__->belongs_to(
  "tipo_visitante",
  "Grata::Model::Schema::Result::TipoVisitante",
  { id => "tipo_visitante" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 visitado

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Personal>

=cut

__PACKAGE__->belongs_to(
  "visitado",
  "Grata::Model::Schema::Result::Personal",
  { persona => "visitado" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "CASCADE",
    on_update     => "CASCADE",
  },
);

=head2 visitante

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Persona>

=cut

__PACKAGE__->belongs_to(
  "visitante",
  "Grata::Model::Schema::Result::Persona",
  { id => "visitante" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 motivo

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Motivo>

=cut

__PACKAGE__->belongs_to(
  "motivo",
  "Grata::Model::Schema::Result::Motivo",
  { id => "motivo" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-16 22:05:49
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:k4i2a8WZdkBSSH9f9qASNA


# You can replace this text with custom content, and it will be preserved on regeneration
1;
