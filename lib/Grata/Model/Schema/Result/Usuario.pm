package Grata::Model::Schema::Result::Usuario;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "EncodedColumn");

=head1 NAME

Grata::Model::Schema::Result::Usuario - Maestro de Usuarios del Sistema

=cut

__PACKAGE__->table("usuario");

=head1 ACCESSORS

=head2 id

  data_type: 'varchar'
  is_nullable: 0
  size: 20

Identificador del usuario

=head2 persona

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

Identificador de los datos personales del usuario

=head2 clave

  data_type: 'varchar'
  is_nullable: 0
  size: 70

Clave de acceso del usuario

=head2 estatus

  data_type: 'varchar'
  default_value: 'ACT'
  is_nullable: 0
  size: 3

Estatus del registro del usuario

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "persona",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "clave",
  { data_type => "varchar", is_nullable => 0, size => 70 },
  "estatus",
  { data_type => "varchar", default_value => "ACT", is_nullable => 0, size => 3 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 persona

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Persona>

=cut

__PACKAGE__->belongs_to(
  "persona",
  "Grata::Model::Schema::Result::Persona",
  { id => "persona" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 usuario_rols

Type: has_many

Related object: L<Grata::Model::Schema::Result::UsuarioRol>

=cut

__PACKAGE__->has_many(
  "usuario_rols",
  "Grata::Model::Schema::Result::UsuarioRol",
  { "foreign.usuario" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 visitas

Type: has_many

Related object: L<Grata::Model::Schema::Result::Visita>

=cut

__PACKAGE__->has_many(
  "visitas",
  "Grata::Model::Schema::Result::Visita",
  { "foreign.usuario" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-16 22:05:49
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ocUubBtTLptodZ9U0gVsMA

# Have the 'clave' column use a SHA-1 hash and 20-byte salt
# with hex encoding; Generate the 'check_password" method
# For use of Passphare at end
__PACKAGE__->add_columns(
    'clave' => {
        encode_column       => 1,
        encode_class => 'Digest',
        encode_args  => { salt_length => 20. },
        encode_check_method => 'check_password',
    },
);

sub new {
	my ( $self, $attrs ) = @_;

	if ($attrs->{id}) {
        $attrs->{id}   =~ s/\A\s+//;    # Eliminar blancos al comienzo - ltrim
        $attrs->{id}   =~ s/\s+\z//;    # Eliminar blancos al final - rtrim
        $attrs->{id}   = lc $attrs->{id} ;    # login en minúscula 
    }

    my $new = $self->next::method($attrs);

	return $new;
}

# You can replace this text with custom content, and it will be preserved on regeneration
1;

__END__
# Cambio a EncodedColumn porque Passphase no esta en Squeeze
#
# Have the 'clave' column use a SHA-1 hash and 20-byte salt
# with RFC 2307 encoding; Generate the 'check_password" method
__PACKAGE__->add_columns(
    'clave' => {
        passphrase       => 'rfc2307',
        passphrase_class => 'SaltedDigest',
        passphrase_args  => {
            algorithm   => 'SHA-1',
            salt_random => 20.
        },
        passphrase_check_method => 'check_password',
    },
);
