package Grata::Model::Schema::Result::Ciudad;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "EncodedColumn");

=head1 NAME

Grata::Model::Schema::Result::Ciudad - Maestro de Ciudades

=cut

__PACKAGE__->table("ciudad");

=head1 ACCESSORS

=head2 id_ciu

  data_type: 'integer'
  is_nullable: 0

Identificador de la ciudad

=head2 nombre

  data_type: 'varchar'
  is_nullable: 0
  size: 75

Nombre de la ciudad

=head2 estado

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

Identificador del estado al que pertenece la ciudad

=head2 estatus

  data_type: 'varchar'
  is_nullable: 0
  size: 3

Estatus del registro de la ciudad

=cut

__PACKAGE__->add_columns(
  "id_ciu",
  { data_type => "integer", is_nullable => 0 },
  "nombre",
  { data_type => "varchar", is_nullable => 0, size => 75 },
  "estado",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "estatus",
  { data_type => "varchar", is_nullable => 0, size => 3 },
);
__PACKAGE__->set_primary_key("id_ciu");

=head1 RELATIONS

=head2 estado

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Estado>

=cut

__PACKAGE__->belongs_to(
  "estado",
  "Grata::Model::Schema::Result::Estado",
  { id_edo => "estado" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 personae

Type: has_many

Related object: L<Grata::Model::Schema::Result::Persona>

=cut

__PACKAGE__->has_many(
  "personae",
  "Grata::Model::Schema::Result::Persona",
  { "foreign.ciudad" => "self.id_ciu" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-16 22:05:49
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:0o78dxJrRecRYbjFjN7VqQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
