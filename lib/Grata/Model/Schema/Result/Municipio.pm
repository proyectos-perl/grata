package Grata::Model::Schema::Result::Municipio;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "EncodedColumn");

=head1 NAME

Grata::Model::Schema::Result::Municipio - Maestro de Municipios

=cut

__PACKAGE__->table("municipio");

=head1 ACCESSORS

=head2 id_mun

  data_type: 'integer'
  is_nullable: 0

Identificador del municipio

=head2 nombre

  data_type: 'varchar'
  is_nullable: 0
  size: 50

Nombre del municipio

=head2 estatus

  data_type: 'varchar'
  is_nullable: 0
  size: 3

Estatus del registro del municipio

=head2 estado

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

Estado al que pertenece el municipio

=cut

__PACKAGE__->add_columns(
  "id_mun",
  { data_type => "integer", is_nullable => 0 },
  "nombre",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "estatus",
  { data_type => "varchar", is_nullable => 0, size => 3 },
  "estado",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id_mun");

=head1 RELATIONS

=head2 estado

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Estado>

=cut

__PACKAGE__->belongs_to(
  "estado",
  "Grata::Model::Schema::Result::Estado",
  { id_edo => "estado" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 personae

Type: has_many

Related object: L<Grata::Model::Schema::Result::Persona>

=cut

__PACKAGE__->has_many(
  "personae",
  "Grata::Model::Schema::Result::Persona",
  { "foreign.municipio" => "self.id_mun" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-16 22:05:49
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:vJh5S3jglAw0H1FepCLgjQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
