package Grata::Model::Schema::Result::Cargo;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "EncodedColumn");

=head1 NAME

Grata::Model::Schema::Result::Cargo - maestro de Cargos del Personal

=cut

__PACKAGE__->table("cargo");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'cargo_id_seq'

Identificador del Cargo

=head2 denominacion

  data_type: 'varchar'
  default_value: 'Sin denominación'
  is_nullable: 0
  size: 50

Denominación del Cargo

=head2 estatus

  data_type: 'varchar'
  default_value: 'ACT'
  is_nullable: 0
  size: 3

Estatus del registro del Cargo
ACT=Activo
INA=Inactivo

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "cargo_id_seq",
  },
  "denominacion",
  {
    data_type => "varchar",
    default_value => "Sin denominaci\xC3\xB3n",
    is_nullable => 0,
    size => 50,
  },
  "estatus",
  { data_type => "varchar", default_value => "ACT", is_nullable => 0, size => 3 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 personals

Type: has_many

Related object: L<Grata::Model::Schema::Result::Personal>

=cut

__PACKAGE__->has_many(
  "personals",
  "Grata::Model::Schema::Result::Personal",
  { "foreign.cargo" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 visitas

Type: has_many

Related object: L<Grata::Model::Schema::Result::Visita>

=cut

__PACKAGE__->has_many(
  "visitas",
  "Grata::Model::Schema::Result::Visita",
  { "foreign.cargo_vtante" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-16 22:18:53
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:zzjcN+VVSg5fYTf/v7Rk6Q


# You can replace this text with custom content, and it will be preserved on regeneration
1;
