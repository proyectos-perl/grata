package Grata::Model::Schema::Result::Personal;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "EncodedColumn");

=head1 NAME

Grata::Model::Schema::Result::Personal - Maestro de Personal

=cut

__PACKAGE__->table("personal");

=head1 ACCESSORS

=head2 persona

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

Identificador de la Persona

=head2 departamento

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

Identificador del Departamento donde labora la Persona

=head2 cargo

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

Identificador del Cargo que ocupa la Persona en la Organización

=head2 ext_telef

  data_type: 'varchar'
  is_nullable: 1
  size: 7

Número de extensión telefónica

=head2 correo_org

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 zona

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

Zona de permanencia de la Persona

=head2 estatus

  data_type: 'varchar'
  default_value: 'ACT'
  is_nullable: 0
  size: 3

Estado de la Persona

=cut

__PACKAGE__->add_columns(
  "persona",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "departamento",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "cargo",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "ext_telef",
  { data_type => "varchar", is_nullable => 1, size => 7 },
  "correo_org",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "zona",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "estatus",
  { data_type => "varchar", default_value => "ACT", is_nullable => 0, size => 3 },
);
__PACKAGE__->set_primary_key("persona");

=head1 RELATIONS

=head2 zona

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Zona>

=cut

__PACKAGE__->belongs_to(
  "zona",
  "Grata::Model::Schema::Result::Zona",
  { id => "zona" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "CASCADE",
    on_update     => "CASCADE",
  },
);

=head2 persona

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Persona>

=cut

__PACKAGE__->belongs_to(
  "persona",
  "Grata::Model::Schema::Result::Persona",
  { id => "persona" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 departamento

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Departamento>

=cut

__PACKAGE__->belongs_to(
  "departamento",
  "Grata::Model::Schema::Result::Departamento",
  { id => "departamento" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "CASCADE",
    on_update     => "CASCADE",
  },
);

=head2 cargo

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Cargo>

=cut

__PACKAGE__->belongs_to(
  "cargo",
  "Grata::Model::Schema::Result::Cargo",
  { id => "cargo" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "CASCADE",
    on_update     => "CASCADE",
  },
);

=head2 visitas

Type: has_many

Related object: L<Grata::Model::Schema::Result::Visita>

=cut

__PACKAGE__->has_many(
  "visitas",
  "Grata::Model::Schema::Result::Visita",
  { "foreign.visitado" => "self.persona" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-16 22:05:49
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:OLLKJ4PuSLx0xXRjCCzTJg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
