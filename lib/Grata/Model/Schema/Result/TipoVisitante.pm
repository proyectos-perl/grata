package Grata::Model::Schema::Result::TipoVisitante;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "EncodedColumn");

=head1 NAME

Grata::Model::Schema::Result::TipoVisitante - Maestro de Tipos de Visitante

=cut

__PACKAGE__->table("tipo_visitante");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'tipo_visitante_id_seq'

Identificador del Tipo de Visitante

=head2 denominacion

  data_type: 'varchar'
  is_nullable: 0
  size: 50

Denominación del Tipo de Visitante

=head2 estatus

  data_type: 'varchar'
  default_value: 'ACT'
  is_nullable: 0
  size: 3

Estatus del Tipo de Visitante
ACT=Activo
INA=inactivo

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "tipo_visitante_id_seq",
  },
  "denominacion",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "estatus",
  { data_type => "varchar", default_value => "ACT", is_nullable => 0, size => 3 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 personae

Type: has_many

Related object: L<Grata::Model::Schema::Result::Persona>

=cut

__PACKAGE__->has_many(
  "personae",
  "Grata::Model::Schema::Result::Persona",
  { "foreign.tipo_visitante" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 visitas

Type: has_many

Related object: L<Grata::Model::Schema::Result::Visita>

=cut

__PACKAGE__->has_many(
  "visitas",
  "Grata::Model::Schema::Result::Visita",
  { "foreign.tipo_visitante" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-16 22:18:53
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:PdhWjQKCXBDMLm0sJftnGg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
