package Grata::Model::Schema::Result::UsuarioRol;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "EncodedColumn");

=head1 NAME

Grata::Model::Schema::Result::UsuarioRol - Relación entre usuarios y roles.

=cut

__PACKAGE__->table("usuario_rol");

=head1 ACCESSORS

=head2 usuario

  data_type: 'varchar'
  is_foreign_key: 1
  is_nullable: 0
  size: 20

Identificador del usuario al que se le asigna el rol

=head2 rol

  data_type: 'varchar'
  is_foreign_key: 1
  is_nullable: 0
  size: 20

Identificador del rol que se le asigna al usuario

=head2 estatus

  data_type: 'varchar'
  default_value: 'ACT'
  is_nullable: 0
  size: 3

Estatus del registro

=cut

__PACKAGE__->add_columns(
  "usuario",
  { data_type => "varchar", is_foreign_key => 1, is_nullable => 0, size => 20 },
  "rol",
  { data_type => "varchar", is_foreign_key => 1, is_nullable => 0, size => 20 },
  "estatus",
  { data_type => "varchar", default_value => "ACT", is_nullable => 0, size => 3 },
);
__PACKAGE__->set_primary_key("usuario", "rol");

=head1 RELATIONS

=head2 usuario

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Usuario>

=cut

__PACKAGE__->belongs_to(
  "usuario",
  "Grata::Model::Schema::Result::Usuario",
  { id => "usuario" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 rol

Type: belongs_to

Related object: L<Grata::Model::Schema::Result::Rol>

=cut

__PACKAGE__->belongs_to(
  "rol",
  "Grata::Model::Schema::Result::Rol",
  { id => "rol" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-16 22:05:49
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kkW6euRLh3cg4tITEIPGtA


# You can replace this text with custom content, and it will be preserved on regeneration
1;
