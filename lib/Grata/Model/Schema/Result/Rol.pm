package Grata::Model::Schema::Result::Rol;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "EncodedColumn");

=head1 NAME

Grata::Model::Schema::Result::Rol - Maestro de Roles del Sistema

=cut

__PACKAGE__->table("rol");

=head1 ACCESSORS

=head2 id

  data_type: 'varchar'
  is_nullable: 0
  size: 20

Identificador del Rol

=head2 denominacion

  data_type: 'varchar'
  default_value: 'Sin Denominación'
  is_nullable: 0
  size: 30

Denominación del Rol

=head2 estatus

  data_type: 'varchar'
  default_value: 'ACT'
  is_nullable: 0
  size: 3

=head2 imagen

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "denominacion",
  {
    data_type => "varchar",
    default_value => "Sin Denominaci\xC3\xB3n",
    is_nullable => 0,
    size => 30,
  },
  "estatus",
  { data_type => "varchar", default_value => "ACT", is_nullable => 0, size => 3 },
  "imagen",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 usuario_rols

Type: has_many

Related object: L<Grata::Model::Schema::Result::UsuarioRol>

=cut

__PACKAGE__->has_many(
  "usuario_rols",
  "Grata::Model::Schema::Result::UsuarioRol",
  { "foreign.rol" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07000 @ 2012-12-17 11:02:09
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:xH2BNPoIkTwP1yMT9oO9YA

sub new {
	my ( $self, $attrs ) = @_;

	if ($attrs->{id}) {
        $attrs->{id}   =~ s/\A\s+//;    # Eliminar blancos al comienzo - ltrim
        $attrs->{id}   =~ s/\s+\z//;    # Eliminar blancos al final - rtrim
        $attrs->{id}   = lc $attrs->{id} ;    # id en minúscula 
    }

    my $new = $self->next::method($attrs);

	return $new;
}

# You can replace this text with custom content, and it will be preserved on regeneration
1;
