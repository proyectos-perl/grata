package Grata::Model::Schema::Result::Estado;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "EncodedColumn");

=head1 NAME

Grata::Model::Schema::Result::Estado - Maestro de Estados y/o Dependencias Federales

=cut

__PACKAGE__->table("estado");

=head1 ACCESSORS

=head2 id_edo

  data_type: 'integer'
  is_nullable: 0

Identificador del estado

=head2 nombre

  data_type: 'varchar'
  is_nullable: 0
  size: 50

Nombre del estado

=head2 estatus

  data_type: 'varchar'
  is_nullable: 0
  size: 3

Estatus del registro del estado

=head2 abrv

  data_type: 'varchar'
  is_nullable: 0
  size: 3

Abreviatura del estado

=cut

__PACKAGE__->add_columns(
  "id_edo",
  { data_type => "integer", is_nullable => 0 },
  "nombre",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "estatus",
  { data_type => "varchar", is_nullable => 0, size => 3 },
  "abrv",
  { data_type => "varchar", is_nullable => 0, size => 3 },
);
__PACKAGE__->set_primary_key("id_edo");

=head1 RELATIONS

=head2 ciudads

Type: has_many

Related object: L<Grata::Model::Schema::Result::Ciudad>

=cut

__PACKAGE__->has_many(
  "ciudads",
  "Grata::Model::Schema::Result::Ciudad",
  { "foreign.estado" => "self.id_edo" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 municipios

Type: has_many

Related object: L<Grata::Model::Schema::Result::Municipio>

=cut

__PACKAGE__->has_many(
  "municipios",
  "Grata::Model::Schema::Result::Municipio",
  { "foreign.estado" => "self.id_edo" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07000 @ 2012-12-17 11:02:09
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:iI/dxeN23fE/C5dE9zOhBw


# You can replace this text with custom content, and it will be preserved on regeneration
1;
