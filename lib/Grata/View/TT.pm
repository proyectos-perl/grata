package Grata::View::TT;

use strict;
use base 'Catalyst::View::TT';

__PACKAGE__->config({
    INCLUDE_PATH => [
        Grata->path_to( 'root', 'src' ),
        Grata->path_to( 'root', 'lib' )
    ],
    PRE_PROCESS  => 'config/main',
    WRAPPER      => 'site/wrapper',
    ERROR        => 'error.tt2',
    TIMER        => 0,
    DEFAULT_ENCODING    => 'utf-8',
    render_die   => 1,
});

=head1 NAME

Grata::View::TT - Catalyst TTSite View

=head1 SYNOPSIS

See L<Grata>

=head1 DESCRIPTION

Catalyst TTSite View.

=head1 AUTHOR

Nelo R. Tovar,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;

