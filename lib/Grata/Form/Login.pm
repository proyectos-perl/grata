package Grata::Form::Login;;
use HTML::FormHandler::Moose;
extends 'HTML::FormHandler';
use namespace::autoclean;

has '+name' => ( default => 'login' );    # id del formulario

has_field 'id' => (
	type     => 'Text',
	label    => 'Usuario',
	title    => 'Usuario',
	required => 1,
	messages => {required => 'Debe suministrar un Usuario'} );

has_field 'clave' => (
	type     => 'Password',
	label    => 'Clave',
	title    => 'Clave del Usuario',
	required => 1,
	messages => {required => 'Debe suministrar la clave del usuario'} );

has_field 'submit' => ( type => 'Submit', value => 'Enviar' );

__PACKAGE__->meta->make_immutable;

1;

=head1 AUTHOR

Nelo R. Tovar, tovar.nelo@gmail.com, 27/10/2011,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
