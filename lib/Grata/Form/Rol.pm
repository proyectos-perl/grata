package Grata::Form::Rol;

use HTML::FormHandler::Moose;
extends 'HTML::FormHandler::Model::DBIC';
use namespace::autoclean;

has '+name' => (default => 'rol');    # id del formulario
has '+item_class' => (default => 'Rol');

has_field 'id' => (type     => 'Text',
                   label    => 'Rol',
                   title    => 'Identificador del Rol',
                   required => 1,
                   messages => {
                           required => 'Debe suministrar un Login para el Usuario'});
has_field 'denominacion' => (type     => 'Text',
                       label    => 'Denominación',
                       title    => 'Denominación del Rol',
                       required => 1,
                       messages => {required => 'Debe introducir la Denominación del Rol'});
has_field 'estatus' => (
               type    => 'Select',
               label   => 'Estatus',
               options => [{value => 'A', label => 'Activo'},
                           {value => 'I', label => 'Inactivo'}],
               required => 0,
);
has_field 'submit' => (type => 'Submit', value => 'Guardar');
has_field 'regresar' => (type => 'Display',
                         html => '<a href="/roles">Regresar</a>');

__PACKAGE__->meta->make_immutable;
1;
