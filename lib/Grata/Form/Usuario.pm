package Grata::Form::Usuario;

use HTML::FormHandler::Moose;
extends 'HTML::FormHandler::Model::DBIC';
use namespace::autoclean;

has '+name' => (default => 'usuario');    # id del formulario
has '+item_class' => (default => 'Usuario');

has_field 'id' => (type     => 'Text',
                   label    => 'Login',
                   title    => 'Login del Usuario',
                   required => 1,
                   messages => {
                           required => 'Debe suministrar un Login para el Usuario'});
has_field 'persona' => (type     => 'Text',
                       label    => 'Cédula',
                       title    => 'Cédula de la Persona',
                       required => 1,
                       messages => {required => 'Debe introducir la Cédula del Usuario'});
has_field 'clave' => (
           type     => 'Password',
           label    => 'Clave',
           title    => 'Clave del usuario',
           required => 1,
           messages => {required => 'Debe suminstrar una Clave para el Usuario'}
);
has_field 'estatus' => (
               type    => 'Select',
               label   => 'Estatus',
               options => [{value => 'ACT', label => 'Activo'},
                           {value => 'INA', label => 'Inactivo'}],
               required => 0,
);
has_field 'submit' => (type => 'Submit', value => 'Guardar');
has_field 'regresar' => (type => 'Display',
                         html => '<a href="/usuarios">Regresar</a>');

__PACKAGE__->meta->make_immutable;
1;
