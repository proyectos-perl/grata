package Grata::Form::Visitas;

use HTML::FormHandler::Moose;
#extends 'HTML::FormHandler::Model::DBIC';
extends 'HTML::FormHandler';
use namespace::autoclean;

has '+name'       => ( default => 'visitas' );    # id del formulario
has '+item_class' => ( default => 'visitas' );
has '+enctype' => ( default => 'multipart/form-data');

has_field 'id' => (
    type => 'Hidden',
    readonly => 1
    );
has_field 'persona' => (
	type      => 'Integer',
	label     => 'Cédula',
	title     => 'Cédula del Visitante',
	minlength => 6,
	maxlength => 8,
	size      => 35,
	required  => 1,
	readonly  => 1,
	messages  => {required => 'Debe introducir la Cédula del Visitante'} );
has_field 'nombre' => (
	type      => 'Text',
	label     => 'Nombre',
	title     => 'Nombre del Visitante',
	size      => 35,
	maxlength => 20,
	required  => 1,
	messages  => {required => 'Debe introducir el Nombre'} );
has_field 'apellido' => (
	type      => 'Text',
	label     => 'Apellido',
	title     => 'Apellido del Visitante',
	size      => 35,
	maxlength => 20,
	required  => 1,
	messages  => {required => 'Debe suminstrar el Apellido'} );
has_field 'sexo' => (
	type    => 'Select',
	label   => 'Sexo',
	options => [
		{value => 'M', label => 'Masculino'},
		{   value => 'F',
			label => 'Femenino'
		}
	],
	required => 1,
	messages => {required => 'Debe suminstrar el Sexo'} );
has_field 'edo_civil' => (
	type    => 'Select',
	label   => 'Estado Civil',
	options => [
		{value => 'S', label => 'Soltero'},
		{value => 'C', label => 'Casado'},
		{value => 'D', label => 'Divorciado'},
		{value => 'V', label => 'Viudo'}
	],
	required => 1,
	messages => {required => 'Debe suminstrar el Edo. Civil'} );
has_field 'nacionalidad' => (
	type     => 'Select',
	label    => 'Sexo',
	options  => [{value => 'V', label => 'Venezolano'}, {value => 'E', label => 'extranjero'}],
	required => 1,
	messages => {required => 'Debe suminstrar la Nacionalidad'} );
has_field 'telefono' => (
	type     => 'Integer',
	label    => 'Teléfono',
	title    => 'Teléfono del Visitante',
	size     => 35,
	required => 1,
	messages => {required => 'Debe suminstrar el Teléfono'} );
has_field 'correo_pers' => (
	type     => 'Email',
	label    => 'Correo Personal',
	title    => 'Correo Personal del Visitante',
	size     => 35,
	required => 0,
	messages => {required => 'Debe suminstrar el Correo Personal'} );
has_field 'procedencia' => (
	type      => 'Text',
	label     => 'Procedencia',
	title     => 'Procedencia de la Visita',
	size      => 35,
	required  => 1,
	messages  => {required => 'Debe introducir Motivo de la Visita'} );
has_field 'cargo' => (
	type     => 'Select',
	label    => 'Cargo',
	title    => 'Cargo del Visitante',
	required => 1,
	messages => {required => 'Debe introducir el Cargo'} );
has_field 'visitado' => (
	type      => 'Select',
	label     => 'Visitado',
	title     => 'Cédula del Visitado',
	required  => 1,
	messages  => {required => 'Debe introducir la Cédula del Visitado'} );
has_field 'motivo' => (
	type      => 'Select',
	label     => 'Motivo',
	title     => 'Motivo de la Visita',
	required  => 1,
	messages  => {required => 'Debe introducir Motivo de la Visita'} );
has_field 'zona' => (
	type      => 'Select',
	label     => 'Zona',
	title     => 'Zona de VisitZona de Visita',
	required  => 1,
	messages  => {required => 'Debe introducir la Cédula del Visitado'} );
has_field 'foto' => (
	type      => 'Upload',
	label     => 'Foto',
	title     => 'Foto del Visitante',
	required  => 0,);
has_field 'nro_carnet' => (
	type      => 'Text',
	label     => 'Nro Carnet',
	title     => 'Nro de Carnet asignado',
	size      => 35,
	maxlength => 5,
	required  => 0 );
has_field 'observaciones' => (
	type      => 'TextArea',
	label     => 'Observación',
	title     => 'Observación a la visita',
	cols      => 40,
	row       => 2,
	required  => 0 );
has_field 'submit' => ( type => 'Submit', value => 'Guardar' );
has_field 'regresar' => (
	type => 'Display',
	html => '<a href="/visitas">Regresar</a>'
);
__PACKAGE__->meta->make_immutable;
1;
