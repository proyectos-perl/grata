package Grata::Form::Denominacion;

use HTML::FormHandler::Moose;
extends 'HTML::FormHandler::Model::DBIC';
use namespace::autoclean;

has '+name' => (default => 'denominacion');    # id del formulario

has_field 'denominacion' => (type     => 'Text',
                       label    => 'Denominación',
                       title    => 'Denominación del item',
                       required => 1,
                       messages => {required => 'Debe introducir la Denominación'});
has_field 'estatus' => (
               type    => 'Select',
               label   => 'Estatus',
               options => [{value => 'ACT', label => 'Activo'},
                           {value => 'INA', label => 'Inactivo'}],
               required => 1,
               messages => {required => 'Debe seleccionar un valor para el Estatus'}
);
has_field 'submit' => (type => 'Submit', value => 'Enviar');
has_field 'regresar' => (type => 'Display',
                         html => '<a href="/">Regresar</a>');

__PACKAGE__->meta->make_immutable;
1;
