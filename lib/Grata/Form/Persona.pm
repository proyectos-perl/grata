package Grata::Form::Persona;

use HTML::FormHandler::Moose;
extends 'HTML::FormHandler::Model::DBIC';
use namespace::autoclean;

has '+name'       => ( default => 'persona' );    # id del formulario
has '+item_class' => ( default => 'Persona' );

has_field 'id' => (
	type     => 'PosInteger',
	label    => 'Cédula ',
	title    => 'Cédula de la Persona',
	minlength => 6,
	maxlength => 8,
	required => 1,
	messages => {
		integer_needed   => 'La cédula solo debe tener números',
		integer_positive => ' ',
		required         => 'Debe suministrar un número de cédula'
	} );
has_field 'nombre' => (
	type      => 'Text',
	label     => 'Nombre',
	title     => 'Nombre de la Persona',
	size      => 35,
	maxlength => 20,
	required  => 1,
	messages  => {required => 'Debe introducir el nombre'} );
has_field 'apellido' => (
	type      => 'Text',
	label     => 'Apellido',
	title     => 'Apellido de la Persona',
	size      => 35,
	maxlength => 20,
	required  => 1,
	messages  => {required => 'Debe suminstrar el Apellido de la Persona'} );
has_field 'sexo' => (
	type    => 'Select',
	label   => 'Sexo',
	options => [
		{value => 'M', label => 'Masculino'},
		{   value => 'F',
			label => 'Femenino'
		}
	],
	required => 1,
	messages => {required => 'Debe suminstrar el Sexo de la Persona'} );
has_field 'edo_civil' => (
	type    => 'Select',
	label   => 'Estado Civil',
	options => [
		{value => 'S', label => 'Soltero'},
		{value => 'C', label => 'Casado'},
		{value => 'D', label => 'Divorciado'},
		{value => 'V', label => 'Viudo'}
	],
	required => 1,
	messages => {required => 'Debe suminstrar el Edo. Civil de la Persona'} );
has_field 'nacionalidad' => (
	type     => 'Select',
	label    => 'Sexo',
	options  => [{value => 'V', label => 'Venezolano'}, {value => 'E', label => 'extranjero'}],
	required => 1,
	messages => {required => 'Debe suminstrar la Nacionalidad de la Persona'} );
has_field 'telefono' => (
	type     => 'Integer',
	label    => 'Teléfono',
	title    => 'Teléfono de la Persona',
	size     => 35,
	required => 1,
	messages => {required => 'Debe suminstrar el Teléfono de la Persona'} );
has_field 'correo_pers' => (
	type     => 'Email',
	label    => 'Correo Personal',
	title    => 'Correo Personal de la Persona',
	size     => 35,
	required => 0,
	messages => {required => 'Debe suminstrar el Correo Personal de la Persona'} );
has_field 'submit' => ( type => 'Submit', value => 'Guardar' );
has_field 'regresar' => (
	type => 'Display',
	html => '<a href="/personas">Regresar</a>'
);

__PACKAGE__->meta->make_immutable;
1;
