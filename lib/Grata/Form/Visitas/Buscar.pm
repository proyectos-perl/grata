package Grata::Form::Visitas::Buscar;

use HTML::FormHandler::Moose;
#extends 'HTML::FormHandler::Model::DBIC';
extends 'HTML::FormHandler';
use namespace::autoclean;
#with 'HTML::FormHandler::Render::Simple';

has '+name'       => ( default => 'visitas_buscar' );    # id del formulario
has '+item_class' => ( default => 'visitas' );
has '+widget_wrapper' => ( default => 'None');
#has '+auto_fieldset' => ( default => 0 );

has_field 'persona' => (
	type      => 'Integer',
	label     => 'Cédula',
	title     => 'Cédula del Visitante',
	minlength => 6,
	maxlength => 8,
	size      => 35,
	required  => 1,
	messages  => {required => 'Debe introducir la Cédula del Visitante'} );
has_field 'submit' => ( type => 'Submit', value => 'Registrar Visita' );

__PACKAGE__->meta->make_immutable;

=head1 AUTHOR

Nelo R. Tovar, tovar.nelo en gmail.com, 6/10/2012

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
1;
