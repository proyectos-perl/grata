package Grata::Form::DatosReporte;

use HTML::FormHandler::Moose;
extends 'HTML::FormHandler';
use namespace::autoclean;

has '+name' => (default => 'datosreporte');    # id del formulario

has_field 'item' => (
	type     => 'Select',
	label    => 'Item',
	title    => 'Seleccione',
	required => 1,
	messages => {required => 'Este campo es obligatorio'}
);
has_field 'finicio' => (
	type     => 'Text',
	label    => 'Fecha Inicio',
	required => 1,
	messages => {required => 'Este campo es obligatorio'}
);
has_field 'ffinal' => (
	type     => 'Text',
	label    => 'Fecha Final',
	required => 1,
	messages => {required => 'Este campo es obligatorio'}
);
has_field 'submit' => (type => 'Submit', value => 'Emitir Reporte');

__PACKAGE__->meta->make_immutable;
1;
