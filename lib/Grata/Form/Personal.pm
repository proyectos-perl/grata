package Grata::Form::Personal;

use HTML::FormHandler::Moose;
extends 'HTML::FormHandler::Model::DBIC';
use namespace::autoclean;
use Data::Dump qw/dump/;

has '+name'       => ( default => 'personal' );    # id del formulario
has '+item_class' => ( default => 'Personal' );

has_field 'persona' => (
	type      => 'Integer',
	label     => 'Cédula',
	title     => 'Cédula del Empleado',
	minlength => 6,
	maxlength => 8,
	size      => 35,
	required  => 1,
	messages  => {required => 'Debe introducir la Cédula del Empleado'} 
);
has_field 'nombre' => (
	type     => 'Text',
	label    => 'Nombre',
	title    => 'Nombre del Empleado',
	size     => 35,
	noupdate => 1,
	readonly => 1
);
has_field 'departamento' => (
	type         => 'Select',
	label_column => 'denominacion',
	label        => 'Departamento',
	title        => 'Departamento de Adscripción',
	required     => 1,
	messages     => {required => 'Debe introducir el Departamento de Adscripción del Empleado'} 
);
has_field 'cargo' => (
	type         => 'Select',
	label_column => 'denominacion',
	label        => 'Cargo',
	title        => 'Cargo del Empleado',
	required     => 1,
	messages     => {required => 'Debe introducir el Cargo del Empleado'} 
);
has_field 'zona' => (
	type         => 'Select',
	label_column => 'denominacion',
	label        => 'Zona',
	title        => 'Zona de Ubicación',
	required     => 1,
	messages     => {required => 'Debe introducir la Zona de Ubicación del Empleado'} 
);
has_field 'ext_telef' => (
	type     => 'Integer',
	label    => 'Extensión',
	title    => 'Extensión Telefónica del Empleado',
	required => 0,
	messages => {required => 'Debe introducir la Cédula del Empleado'} 
);
has_field 'correo_org' => (
	type      => 'Email',
	label     => 'Correo Org.',
	title     => 'Correo Organizacional',
	size      => 35,
	maxlength => 30,
	required  => 0,
	messages  => {required => 'Debe introducir Correo Organizacional del Empleado'} 
);
has_field 'estatus' => (
	type     => 'Select',
	label    => 'Estatus',
	options  => [{value => 'ACT', label => 'Activo'}, {value => 'INA', label => 'Inactivo'}],
	required => 0,
);
has_field 'submit' => ( type => 'Submit', value => 'Guardar' );
has_field 'regresar' => (
	type => 'Display',
	html => '<a href="/personal">Regresar</a>'
);

sub init_value_nombre{
   my ($self, $field, $rs) = @_;

   eval {$rs->persona->nombre_completo };
   return $rs->persona->nombre_completo if (!$@);
   return undef;

};

__PACKAGE__->meta->make_immutable;
1;
