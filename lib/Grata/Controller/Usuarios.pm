package Grata::Controller::Usuarios;
use Moose;
use Grata::Form::Usuario;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

has 'formulario' => (
	isa     => 'Grata::Form::Usuario',
	is      => 'rw',
	lazy    => 1,
	default => sub { Grata::Form::Usuario->new } );

=head1 NAME

Grata::Controller::Usuarios - Controlador de Usuarios

=head1 DESCRIPTION

Controlador de Usuarios.

=head1 METHODS

=cut

=head2 index

=cut

sub index : Path : Args(0) {
	my ( $self, $c ) = @_;

	$c->stash( template => 'usuarios/index.tt2' );
}

=head2 agregar

Agregar un Usuario a la Base de Datos

=cut

sub agregar : Local {
    my ($self, $c) = @_;
    my $usuarios = $c->model('BD::Usuario')->new_result({});

    eval {
        $self->formulario->process(item   => $usuarios,
                                   params => $c->req->parameters);
    };

    if ($@) {
        $c->stash->{mensaje_error} = 'El Usuario no pudo ser agregado';
        $c->log->error($@);
    } elsif (   $self->formulario->ran_validation
             && $self->formulario->validated) {
        $c->flash(  mensaje_estatus => 'Usuario &#171;<i>'
                  . $self->formulario->field('id')->value
                  . '</i>&#187; agregado con &eacute;xito');
        $c->response->redirect($c->uri_for('/usuarios'));
    }

    $c->stash(template => 'usuarios/agregar.tt2',
              form     => $self->formulario);

}    # ----------  end of subroutine agregar  ----------

=head2 editar

Editar una Usuario a la Base de Datos

=cut

sub editar : Local {
    my ($self, $c, $id) = @_;

    #$self->formulario->field('id')->html_attr({readonly => 1});
    eval {
        $self->formulario->process(item_id => $id,
                                   params  => $c->req->parameters,
                                   schema  => $c->model('BD')->schema);
    };

    unless ($self->formulario->field('id')->value) {
        $c->log->debug('id=' . $self->formulario->field('id')->value);
        $c->flash(mensaje_error =>
                "El Usuario  &#171;<i>$id</i>&#187; no existe");
        $c->log->warn("Se trató de EDITAR un Usuario que no existe:$id");
        $c->response->redirect($c->uri_for('/usuarios'));
        return;
    }

    if ($@) {
        $c->stash->{mensaje_error} = 'El Usuario NO PUDO ser modificado';
        $c->log->error($@);
    } elsif ($self->formulario->ran_validation && $self->formulario->validated)
    {
        $c->flash->{mensaje_estatus} =
            "Usuario &#171;<i>$id</i>&#187; "
          . "modificado con &eacute;xito";
        $c->response->redirect($c->uri_for('/usuarios'));
    }

    $c->stash(template => 'usuarios/editar.tt2', form => $self->formulario);

}    # ----------  end of subrusuario editar  ----------

sub borrar : Local {
    my ( $self, $c, $id )	= @_;

    eval{$c->model('BD::Usuario')->find($id)->delete;};

    if($@) {
        $c->res->status(404);
        $c->res->body('nook');
    }else{
        $c->res->status(200);
        $c->res->body('ok');
    }
} ## --- end sub borrar

=head2 roles 

Manejo de los roles de un usuario

=cut

sub roles : Local {
    my ( $self, $c, $id ) = @_;

    if ($id) {
        $c->stash->{usuario} = $c->model('BD::Usuario')->find($id);
        $c->stash->{roles_asignados} = [$c->model('BD::UsuarioRol')->search( {usuario => $id} )];
        $c->stash->{roles_por_asignar} =
        [$c->model('BD::Rol')
            ->search( {id => \"not in (select rol from usuario_rol where usuario = '$id')"} )];
        $c->stash->{template} = 'usuarios/roles.tt2';
    } else{
        $c->detach('index');
    }

}

=head2 eliminar_rol 

Elimina un rol de un usuario

=cut

sub eliminar_rol : Local {
    my ( $self, $c, $usuario_id, $rol_id ) = @_;

    my $usuario_rol =
      $c->model('BD::UsuarioRol')->find( {usuario => $usuario_id, rol => $rol_id} );

    if ($usuario_rol) {
        $usuario_rol->delete();
    } else {
        $c->stash->{mensaje_error} =
          "El usuario &#171;<i>$usuario_id</i>&#187; no tiene el rol &#171;<i>$rol_id</i>&#187;";
        $c->log->warn(
            "El usuario &#171;<i>$usuario_id</i>&#187; no tiene el rol &#171;<i>$rol_id</i>&#187;");
    }

    $c->forward( 'roles', ( $self, $c, $usuario_id ) );

}

=head2 agregar_rol 

Agrega un rol a un usuario

=cut

sub agregar_rol : Local {
    my ( $self, $c, $usuario_id, $rol_id ) = @_;

    my $usuario_rol =
      $c->model('BD::UsuarioRol')->find( {usuario => $usuario_id, rol => $rol_id} );

    eval { $c->model('BD::UsuarioRol')->create( {usuario => $usuario_id, rol => $rol_id} ) };
    if ($@) {
        $c->stash->{mensaje_error} =
          "El rol &#171;<i>$rol_id</i>&#187; no se pudo asignar al usuario &#171;<i>$usuario_id</i>&#187; ";
        $c->log->warn(
            "El rol &#171;<i>$rol_id</i>&#187; no se pudo asignar al usuario &#171;<i>$usuario_id</i>&#187;: $@"
        );
    }

    $c->forward( 'roles', ( $self, $c, $usuario_id ) );

}

sub cargar_dataTable : Local {
	my ( $self, $c ) = @_;
	my %where;
	my $orden = 'me.id';

	$where{-or} = [
			'me.id'   => {'like', lc($c->req->param('sSearch')) . '%'},
			'persona.nombre' => {'like', $c->req->param('sSearch') . '%'}];

	$c->stash(
		no_wrapper    => 1,
		template      => 'usuarios/cargar_dataTable.tt2',
		sEcho         => $c->req->param('sEcho'),            # Se debe pasar directo
		iTotalRecords => $c->model('BD::Usuario')->count,    # Cantidad total de Registros
		iTotalDisplayRecords =>
		  $c->model('BD::Usuario')->search( {%where}, 
		      {prefetch =>'persona'} )->count,    # Cantidad de Registros Filtrados
		usuarios => [
			$c->model('BD::Usuario')->search(
				{%where},
				{  prefetch => 'persona',
				    page => $c->req->param('iDisplayStart')
					  || 1,                                       # Página a mostrar
					rows => $c->req->param('iDisplayLength')
					  || 10,                                      # Nro de Registros a recuperar
					order_by => $orden
				} )] );
}

sub autoc : Local {
	my ( $self, $c ) = @_;
	my %where;
	my $orden = 'me.id';

	$c->stash(
		no_wrapper    => 1,
		template      => 'usuarios/autocomplete.tt2',
		personas => [
			$c->model('BD::Persona')->search(
                #{'me.nombre'   => {'like', $c->req->param('term') . '%'}},
				{'me.id::text'   => {'like', $c->req->param('term') . '%'}},
				{  order_by => $orden } )] );
}

=head1 AUTHOR

Nelo R. Tovar, tovar.nelo@gmail.com, 26/08/2012

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
