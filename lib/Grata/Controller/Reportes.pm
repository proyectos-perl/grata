package Grata::Controller::Reportes;
use Moose;
use Grata::Form::DatosReporte;
use namespace::autoclean;

has 'formulario' => (
	isa     => 'Grata::Form::DatosReporte',
	is      => 'rw',
	lazy    => 1,
	default => sub { Grata::Form::DatosReporte->new() }
);

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

Grata::Controller::Reportes - Controlador de Reportes

=head1 DESCRIPTION

Controlador para la Emisión de Reportes.

=head1 METHODS

=cut

=head2 index

Página inicial del Módulo. Pasa el control a la acción por_fechas.

=cut

sub index : Path : Arg(0) {
	my ($self, $c) = @_;
	$c->detach('por_fechas');
}

=head1 por_fechas

Reporte de Visitantres por fechas

=cut

sub por_fechas : Local {
	my ($self, $c) = @_;

	if ($self->formulario->process(params => $c->req->parameters)) {
		emitir_reporte_por_fechas($self, $c);
	} else {
		$self->formulario->field('item')->inactive(1);
		$c->stash(
			template     => 'reportes/datosreporte.tt2',
			tipo_reporte => 'Fechas',
			form         => $self->formulario
		);
	}
}

sub emitir_reporte_por_fechas {
	my ($self, $c) = @_;
	my %where = (
		'fecha_visita' => {
			-between => [
				$self->formulario->field('finicio')->value,
				$self->formulario->field('ffinal')->value
			]
		}
	);

	$c->stash(
		finicio => $self->formulario->field('finicio')->value,
		ffinal  => $self->formulario->field('ffinal')->value,
		datos   => [
			$c->model('BD::Visita')->search(
				{%where},
				{   prefetch => [
						qw/visitante visitado procedencia motivo
						  tipo_visitante/
					],
					order_by => 'me.id'
				}
			)
		],
		no_wrapper => 1,
		template   => 'reportes/por_fechas_pdf.tt2'
	);

	$c->res->content_type('application/pdf');
}

sub cargar_opciones {
	my ($rs, $campo_value, $campo_label) = @_;

	return [
		(   {value => 'TODOS', label => 'Todos'},
			map {
				{   value => $_->$campo_value,
					label => $_->get_column($campo_label)
				}
			  } $rs->search({estatus => 'ACT'}, {order_by => $campo_label})
		)
	];
}

=head1 AUTHOR

Nelo R. Tovar, tovar.nelo@gmail.com, 17/12/2012.

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
