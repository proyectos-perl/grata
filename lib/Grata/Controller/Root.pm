package Grata::Controller::Root;
use Moose;
use Grata::Form::Login;
use namespace::autoclean;
use Data::Dump qw(dump);

BEGIN { extends 'Catalyst::Controller' }

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config(namespace => '');

=head1 NAME

Grata::Controller::Root - Root Controller for Grata

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=head2 index

The root page (/)

=cut

sub index : Path : Args(0) {
    my ($self, $c) = @_;

    $c->stash(template => 'welcome.tt2');
}

=head2 default

Standard 404 error page

=cut

sub default :Path {
    my ( $self, $c ) = @_;

    $c->log->warn('Página no encontrada');
    $c->response->status(404);
    $c->stash(template => 'site/404.tt2');
}

=head2 auto

Chequea si el usuario tiene una sesion activa, en caso contrario redirige el
flujo del programa a la solicitud de login

=cut

sub auto : Private {
    my	( $self, $c ) = @_;

    if ($c->controller eq $c->controller('Login')) {
        return 1;
    }

    if (!$c->user_exists() ) {
        $c->log->warn('Intento de acceso sin sesión activa');
        $c->res->redirect($c->uri_for('/login'));
        return 0;
    }

    return 1;
}	# ----------  end of subroutine auto  ----------

=head2 login

Autenticar un Usuario

=cut

sub login : Global {
    my	( $self, $c ) = @_;
    my $formulario = Grata::Form::Login->new;

    $formulario->process(params => $c->request->parameters);
    if ($formulario->validated) {
        $c->stash(mensaje => 'Valido');
    }else{
        $c->stash(mensaje => 'Errado');
    }

    $c->stash(template => 'site/login.tt2', form => $formulario);
}	# ----------  end of subroutine login  ----------

=head2 access_denied

Manejo del acceso negado

=cut

sub access_denied : Private {
    my ( $self, $c ) = @_;

    $c->stash->{template} = 'site/acceso_negado.tt2';
}


=head2 end

Attempt to render a view, if needed.

=cut

sub end : ActionClass('RenderView') {}

=head1 AUTHOR

Nelo R. Tovar, tovar.nelo@gmail.com,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
