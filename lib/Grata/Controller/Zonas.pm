package Grata::Controller::Zonas;
use Moose;
use Grata::Form::Denominacion;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

has 'formulario' => (
	isa     => 'Grata::Form::Denominacion',
	is      => 'rw',
	lazy    => 1,
	default => sub { Grata::Form::Denominacion->new() } );

=head1 NAME

Grata::Controller::Zonas - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

=head2 base
    
Acción con elementos comunes a las otras acciones
    
=cut

sub base : Chained('/') : PathPart('zonas') : CaptureArgs(0) {
	my ( $self, $c ) = @_;
    my $modulo = { nombre => 'Zonas', item => 'Zona', accion => 'Catálogo'};

	# colocar en el stash valores comunes
	$c->stash(
		modulo   => $modulo,
		catalogo => $c->model('BD::Zona'),
		form     => $self->formulario,
		inicio   => $c->uri_for('/zonas') );

	# url para regresar al pagina principal del controlador
	$self->formulario->field('regresar')
	  ->html( '<a href="' . $c->stash->{inicio} . '">Regresar</a>' );
}

=head2 index

Acción de entrada al Módulo

=cut

sub index : Chained('base') : PathPart('') : Args(0) {
	my ( $self, $c ) = @_;

	$c->stash( template => 'denominacion/index.tt2' );
}

=head2 agregar

Agregar una Zona a la Base de Datos

=cut

sub agregar : Chained('base') : PathPart('agregar') : Arg(0) {
	my ( $self, $c ) = @_;
	my $zona = $c->stash->{catalogo}->new_result( {} );

	eval { $self->formulario->process( item => $zona, params => $c->req->parameters ); };

	if ($@) {
		$c->stash->{mensaje_error} = 'La Zona no pudo ser agregada';
		$c->log->error($@);
	} elsif ( $self->formulario->ran_validation
		&& $self->formulario->validated ) {
		$c->flash( mensaje_estatus => 'La Zona &#171;<i>'
			  . $self->formulario->field('denominacion')->value
			  . '</i>&#187; agregada con &eacute;xito' );
		$c->response->redirect( $c->stash->{inicio} );
	}

	$c->stash( template => 'denominacion/agregar.tt2');
    $c->stash->{modulo}{accion} = 'Agregar';

}    # ----------  end of subroutine agregar  ----------

=head2 editar  

Editar una zona a la Base de Datos

=cut

sub editar : Chained('base') : PathPart('editar') : Arg(1) {
	my ( $self, $c, $id ) = @_;
	my $zona;

	if ( $zona = $c->stash->{catalogo}->find($id) ) {

		eval { $self->formulario->process( item => $zona, params => $c->req->parameters ); };

		if ($@) {
			$c->stash->{mensaje_error} = 'La Zona NO PUDO ser modificada';
			$c->log->error($@);
		} elsif ( $self->formulario->ran_validation && $self->formulario->validated ) {
			$c->flash->{mensaje_estatus} =
			    'Zona &#171;<i>'
			  . $self->formulario->field('denominacion')->value
			  . '</i>&#187; modificada con &eacute;xito';
			$c->response->redirect( $c->stash->{inicio} );
		}

	} else {
		$c->flash( mensaje_error => "La Zona  &#171;<i>$id</i>&#187; no existe" );
		$c->log->warn("Se trató de EDITAR una Zona que no existe:$id");
		$c->response->redirect( $c->stash->{inicio} );
		return;
	}

	$c->stash( template => 'denominacion/agregar.tt2' );
    $c->stash->{modulo}{accion} = 'Editar';

}    # ----------  end of subroutine editar  ----------

=head2 borrar

Borrar una Zona

=cut

sub borrar : Chained('base') : PathPart('borrar') : Arg(1) {
	my ( $self, $c, $id ) = @_;

	eval { $c->stash->{catalogo}->find($id)->delete; };

	if ($@) {
		$c->res->status(404);
		$c->res->body('nook');
	} else {
		$c->res->status(200);
		$c->res->body('ok');
	}
}    ## --- end sub borrar

=head1 AUTHOR

Nelo R. Tovar, tovar.nelo en gmail.com, 20/12/2011

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
