package Grata::Controller::Cargos;
use Moose;
use Grata::Form::Denominacion;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

has 'formulario' => (
	isa     => 'Grata::Form::Denominacion',
	is      => 'rw',
	lazy    => 1,
	default => sub { Grata::Form::Denominacion->new() } );

=head1 NAME

Grata::Controller::Cargos - Controlador del Módulo de Cargos

=head1 DESCRIPTION

Controlador del Módulo de Cargos.

=head1 METHODS

=cut

=head2 base
    
Acción con elementos comunes a las otras acciones
    
=cut

sub base : Chained('/') : PathPart('cargos') : CaptureArgs(0) {
	my ( $self, $c ) = @_;
    my $modulo = { nombre => 'Cargos', item => 'Cargo', accion => 'Catálogo'};

	# colocar en el stash valores comunes
	$c->stash(
		modulo   => $modulo,
		catalogo => $c->model('BD::Cargo'),
		form     => $self->formulario,
		inicio   => $c->uri_for('/cargos') );

	# url para regresar al pagina principal del controlador
	$self->formulario->field('regresar')
	  ->html( '<a href="' . $c->stash->{inicio} . '">Regresar</a>' );
}

=head2 index

Acción de entrada al Módulo de cargos

=cut

sub index : Chained('base') : PathPart('') : Args(0) {
	my ( $self, $c ) = @_;

	$c->stash( template => 'denominacion/index.tt2' );
}

=head2 agregar

Agregar un Cargo a la Base de Datos

=cut

sub agregar : Chained('base') : PathPart('agregar') : Arg(0) {
	my ( $self, $c ) = @_;
	my $cargo = $c->stash->{catalogo}->new_result( {} );

	eval { $self->formulario->process( item => $cargo, params => $c->req->parameters ); };

	if ($@) {
		$c->stash->{mensaje_error} = 'El Cargo no pudo ser agregado';
		$c->log->error($@);
	} elsif ( $self->formulario->ran_validation
		&& $self->formulario->validated ) {
		$c->flash( mensaje_estatus => 'El Cargo &#171;<i>'
			  . $self->formulario->field('denominacion')->value
			  . '</i>&#187; agregado con &eacute;xito' );
		$c->response->redirect( $c->stash->{inicio} );
	}

	$c->stash( template => 'denominacion/agregar.tt2');
    $c->stash->{modulo}{accion} = 'Agregar';

}    # ----------  end of subroutine agregar  ----------

=head2 editar  

Editar un Cargo a la Base de Datos

=cut

sub editar : Chained('base') : PathPart('editar') : Arg(1) {
	my ( $self, $c, $id ) = @_;
	my $cargo;

	if ( $cargo = $c->stash->{catalogo}->find($id) ) {

		eval { $self->formulario->process( item => $cargo, params => $c->req->parameters ); };

		if ($@) {
			$c->stash->{mensaje_error} = 'El Cargo NO PUDO ser modificado';
			$c->log->error($@);
		} elsif ( $self->formulario->ran_validation && $self->formulario->validated ) {
			$c->flash->{mensaje_estatus} =
			    'Cargo &#171;<i>'
			  . $self->formulario->field('denominacion')->value
			  . '</i>&#187; modificado con &eacute;xito';
			$c->response->redirect( $c->stash->{inicio} );
		}

	} else {
		$c->flash( mensaje_error => "El Cargo  &#171;<i>$id</i>&#187; no existe" );
		$c->log->warn("Se trató de EDITAR un Cargo que no existe:$id");
		$c->response->redirect( $c->stash->{inicio} );
		return;
	}

	$c->stash( template => 'denominacion/agregar.tt2' );
    $c->stash->{modulo}{accion} = 'Editar';

}    # ----------  end of subroutine editar  ----------

=head2 borrar

Borrar un Cargo

=cut

sub borrar : Chained('base') : PathPart('borrar') : Arg(1) {
	my ( $self, $c, $id ) = @_;

	eval { $c->stash->{catalogo}->find($id)->delete; };

	if ($@) {
		$c->res->status(404);
		$c->res->body('nook');
	} else {
		$c->res->status(200);
		$c->res->body('ok');
	}
}    ## --- end sub borrar

=head1 AUTHOR

Nelo R. Tovar, tovar.nelo en gmail.com, 23/09/2011

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
