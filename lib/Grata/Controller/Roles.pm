package Grata::Controller::Roles;
use Moose;
use Grata::Form::Rol;
use Data::Dump qw/dump/;
use namespace::autoclean;

BEGIN {extends 'Catalyst::Controller'; }

has 'formulario' => (
	isa     => 'Grata::Form::Rol',
	is      => 'rw',
	lazy    => 1,
	default => sub { Grata::Form::Rol->new } );

=head1 NAME

Grata::Controller::Roles - Controlador de Roles

=head1 DESCRIPTION

Controlador de Roles.

=head1 METHODS

=cut


=head2 index

Página principal del Controlador de Roles

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash( template => 'roles/index.tt2'); 
}


=head2 agregar

Agregar un Rol a la Base de Datos

=cut

sub agregar : Local {
    my ($self, $c) = @_;
    my $rol = $c->model('BD::Rol')->new_result({});

    eval {
        $self->formulario->process(item   => $rol,
                                   params => $c->req->parameters);
    };

    if ($@) {
        $c->stash->{mensaje_error} = 'El Rol no pudo ser agregado';
        $c->log->error($@);
    } elsif (   $self->formulario->ran_validation
             && $self->formulario->validated) {
        $c->flash(  mensaje_estatus => 'Rol &#171;<i>'
                  . $self->formulario->field('id')->value
                  . '</i>&#187; agregado con &eacute;xito');
        $c->response->redirect($c->uri_for('/roles'));
    }

    $c->stash(template => 'roles/agregar.tt2',
              form     => $self->formulario);

}    # ----------  end of subroutine agregar  ----------

=head2 editar

Editar un Rol a la Base de Datos

=cut

sub editar : Local {
    my ($self, $c, $id) = @_;

    #$self->formulario->field('id')->html_attr({readonly => 1});
    eval {
        $self->formulario->process(item_id => $id,
                                   params  => $c->req->parameters,
                                   schema  => $c->model('BD')->schema);
    };

    unless ($self->formulario->field('id')->value) {
        $c->log->debug('id=' . $self->formulario->field('id')->value);
        $c->flash(mensaje_error =>
                "El Rol  &#171;<i>$id</i>&#187; no existe");
        $c->log->warn("Se trató de EDITAR un Rol que no existe:$id");
        $c->response->redirect($c->uri_for('/roles'));
        return;
    }

    if ($@) {
        $c->stash->{mensaje_error} = 'El Rol NO PUDO ser modificado';
        $c->log->error($@);
    } elsif ($self->formulario->ran_validation && $self->formulario->validated)
    {
        $c->flash->{mensaje_estatus} =
            "Rol &#171;<i>$id</i>&#187; "
          . "modificado con &eacute;xito";
        $c->response->redirect($c->uri_for('/roles'));
    }

    $c->stash(template => 'roles/editar.tt2', form => $self->formulario);

}    # ----------  end of subrusuario editar  ----------

sub borrar : Local {
    my ( $self, $c, $id )	= @_;

    eval{$c->model('BD::Rol')->find($id)->delete;};

    if($@) {
        $c->res->status(404);
        $c->res->body('nook');
    }else{
        $c->res->status(200);
        $c->res->body('ok');
    }
} ## --- end sub borrar

=head2 usuarios  

Manejo de los usuarios y rol

=cut

sub usuarios :Local {
    my ( $self, $c, $rol_id ) = @_;

    if ($rol_id) {
        $c->stash->{rol} = $c->model('BD::Rol')->find($rol_id);
        $c->stash->{usuarios_asignados} =
            [$c->model('BD::UsuarioRol')->search({rol => $rol_id})];
        $c->stash->{usuarios_por_asignar} = [$c->model('BD::Usuario')->search({
            id => 
                \"not in (select usuario from usuario_rol where rol = '$rol_id')"
        })];
        $c->stash->{template} = 'roles/usuarios.tt2';
    }else{
        $c->detach('index');
    }

}

=head2 eliminar_usuario 

Elimina un usuario de un rol

=cut

sub eliminar_usuario :Local {
    my ( $self, $c, $rol_id, $usuario_id, ) = @_;

    my $usuario_rol = $c->model('BD::UsuarioRol')->find({
            rol => $rol_id, usuario => $usuario_id});

    if ($usuario_rol){
        $usuario_rol->delete();
    }else{
        $c->stash->{mensaje_error} = 
            "El rol &#171;<i>$rol_id</i>&#187; no tiene el usuario &#171;<i>$usuario_id</i>&#187; asignado";
        $c->log->warn("El rol &#171;<i>$rol_id</i>&#187; no tiene el usuario &#171;<i>$usuario_id</i>&#187; asignado");
    }

    $c->forward('usuarios', ($self, $c, $rol_id));

}

=head2 agregar_usuario 

Agrega un usuario a un rol

=cut

sub agregar_usuario :Local {
    my ( $self, $c, $rol_id, $usuario_id, ) = @_;

    my $usuario_rol = $c->model('BD::UsuarioRol')->find({
            rol => $rol_id, usuario => $usuario_id});

    eval{$c->model('BD::UsuarioRol')->create({rol => $rol_id,
        usuario => $usuario_id})};
    
    if ($@){
        $c->stash->{mensaje_error} = 
            "El usuario &#171;<i>$usuario_id</i>&#187; no se pudo asignar al rol  &#171;<i>$rol_id</i>&#187; ";
        $c->log->warn("El usuario &#171;<i>$usuario_id</i>&#187; no se pudo asignar al rol  &#171;<i>$rol_id</i>&#187; "
);
    }

    $c->forward('usuarios', ($self, $c, $rol_id));

}


sub cargar_dataTable : Local {
	my ( $self, $c ) = @_;
	my %where;
	my $orden = 'me.id';

	$where{-or} = [
			'me.id'   => {'like', lc($c->req->param('sSearch')) . '%'},
			'me.denominacion' => {'like', $c->req->param('sSearch') . '%'}];


	$c->stash(
		no_wrapper    => 1,
		template      => 'roles/cargar_dataTable.tt2',
		sEcho         => $c->req->param('sEcho'),            # Se debe pasar directo
		iTotalRecords => $c->model('BD::Rol')->count,    # Cantidad total de Registros
		iTotalDisplayRecords =>
		  $c->model('BD::Rol')->search( {%where})->count,    # Cantidad de Registros Filtrados
		roles => [
			$c->model('BD::Rol')->search(
				{%where},
				{  offset => $c->req->param('iDisplayStart')
                    	  || 0,                                       # Página a mostrar
					rows => $c->req->param('iDisplayLength')
					  || 10,                                      # Nro de Registros a recuperar
					order_by => $orden
				} )] );
}


=head1 AUTHOR

Nelo R. Tovar, tovar.nelo@gmail.com, 17/09/2012.

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
