package Grata::Controller::Personal;
use Moose;
use Grata::Form::Personal;
use namespace::autoclean;

BEGIN {extends 'Catalyst::Controller'; }

has 'formulario' => (
	isa     => 'Grata::Form::Personal',
	is      => 'rw',
	lazy    => 1,
	default => sub { Grata::Form::Personal->new } );

=head1 NAME

Grata::Controller::Personal - Controlador de Personal

=head1 DESCRIPTION

Controlador de Personal.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash(template => 'personal/index.tt2');
}

=head2 agregar

Agregar un Personal a la Base de Datos

=cut

sub agregar : Local {
    my ($self, $c) = @_;
    my $personal = $c->model('BD::Personal')->new_result({});

    # Solo dejar los digitos en estos campos
    foreach  (qw/persona/) {
         $c->req->parameters->{$_} = $1 
            if (defined $c->req->parameters->{$_} && $c->req->parameters->{$_} =~ /^(\d+)/); 
    };

    eval {
        $self->formulario->process(item   => $personal,
                               params => $c->req->parameters);
    };

    if ($@) {
        $c->log->error($@);
    } elsif (   $self->formulario->ran_validation
             && $self->formulario->validated) {
        $c->flash(  mensaje_estatus => 'Empleado &#171;<i>'
                  . $self->formulario->field('persona')->value
                  . '</i>&#187; agregado con &eacute;xito');
        $c->response->redirect($c->uri_for('/personal'));
    }

    $c->stash(template => 'personal/agregar.tt2',
              form     => $self->formulario);

}    # ----------  end of subroutine agregar  ----------

=head2 editar

Editar una Usuario a la Base de Datos

=cut

sub editar : Local {
    my ($self, $c, $id) = @_;

    eval {
        $self->formulario->process(item_id => $id,
                                   params  => $c->req->parameters,
                                   schema  => $c->model('BD')->schema);
    };

    unless ($self->formulario->field('persona')->value) {
        $c->flash(mensaje_error =>
                "El Empleado  &#171;<i>$id</i>&#187; no existe");
        $c->log->warn("Se trató de EDITAR un empleado que no existe:$id.");
        $c->response->redirect($c->uri_for('/personal'));
        return;
    }

    if ($@) {
        $c->stash->{mensaje_error} = 'El Empleado NO PUDO ser modificado';
        $c->log->error($@);
    } elsif ($self->formulario->ran_validation && $self->formulario->validated)
    {
        $c->flash->{mensaje_estatus} =
            "Empleado &#171;<i>$id</i>&#187; "
          . "modificado con &eacute;xito";
        $c->response->redirect($c->uri_for('/personal'));
    }else{
        $self->formulario->field('persona')->readonly(1);
        $self->formulario->field('persona')->noupdate(1);
    }

    $c->stash(template => 'personal/editar.tt2', form => $self->formulario);

}    # ----------  end of subrusuario editar  ----------

sub borrar : Local {
    my ( $self, $c, $id )	= @_;

    eval{$c->model('BD::Personal')->find($id)->delete;};

    if($@) {
        $c->log->error("error al tratar de borra el Personal $id" . $@); 
        $c->res->status(404);
        $c->res->body('nook');
    }else{
        $c->res->status(200);
        $c->res->body('ok');
    }
} ## --- end sub borrar


sub cargar_dataTable : Local {
	my ( $self, $c ) = @_;
	my %where;
	my $orden = 'me.persona';

	$where{-or} = [
			'persona.nombre' => {'like', $c->req->param('sSearch') . '%'}];

	$c->stash(
		no_wrapper    => 1,
		template      => 'personal/cargar_dataTable.tt2',
		sEcho         => $c->req->param('sEcho'),            # Se debe pasar directo
		iTotalRecords => $c->model('BD::Personal')->count,    # Cantidad total de Registros
		iTotalDisplayRecords =>
		  $c->model('BD::Personal')->search( {%where},
		    {prefetch => 'persona'})->count,    # Cantidad de Registros Filtrados
		personal => [
			$c->model('BD::Personal')->search(
				{%where},
				{  prefetch => [ qw /persona departamento cargo zona /],
				    page => $c->req->param('iDisplayStart')
					  || 1,                                       # Página a mostrar
					rows => $c->req->param('iDisplayLength')
					  || 10,                                      # Nro de Registros a recuperar
					order_by => $orden
				} )] );
}

sub autoc_pers : Local {
	my ( $self, $c ) = @_;
	my $orden = 'me.id';

	$c->stash(
		no_wrapper    => 1,
		template      => 'personal/autocomplete_pers.tt2',
		personas => [
			$c->model('BD::Persona')->search(
				{'me.id::text'   => {'like', $c->req->param('term') . '%'}},
				{  order_by => $orden } )] );
}

sub autoc : Local {
	my ( $self, $c, $id_tabla ) = @_;
	my %tablas = (departamento => 'BD::Departamento',
	    cargo => 'BD::Cargo', zona => 'BD::Zona');

	$c->stash(
		no_wrapper    => 1,
		template      => 'personal/autocomplete.tt2',
		datos => [
			$c->model($tablas{$id_tabla})->search(
				{'me.denominacion'   => {'like', $c->req->param('term') . '%'}},
				{  order_by => 'denominacion' } )] );
}


=head1 AUTHOR

Nelo R. Tovar, tovar.nelo@gmail.com, 26/09/2012.

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
