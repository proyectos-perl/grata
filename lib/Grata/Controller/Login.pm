package Grata::Controller::Login;
use Moose;
use Grata::Form::Login;
use namespace::autoclean;

BEGIN {extends 'Catalyst::Controller'; }

=head1 NAME

Grata::Controller::Login - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->go('login');
}

=head2 login

Autenticar un usario

=cut

sub login : Private {
    my ($self, $c) = @_;
    my $formulario = Grata::Form::Login->new();

    $c->stash(template => 'login/login.tt2', form => $formulario);

    if ($formulario->process( params => $c->req->parameters)){
        if ($c->authenticate({ id => lc($formulario->field('id')->value), 
            clave => $formulario->field('clave')->value, estatus => 'ACT'})) {
            $c->log->debug("Autentico usuario:" . $c->user->persona->nombre_completo); 
            if ($c->user->roles){
                $c->res->redirect($c->uri_for('/visitas'));
                return;
            }else{
                $c->flash(mensaje_error => 'Usted no est&aacute; autorizado para usar el sistema');   
                $c->detach('logout');                
            }
        }else{
            $c->stash(mensaje_error => 'Login y/o Clave incorrectos');
        }
    }
}

=head2 logout

Salir del Sistema

=cut

sub logout : Global {
    my ($self, $c) = @_;

    $c->logout;
    $c->res->redirect($c->uri_for('/login'));
}


=head1 AUTHOR

Nelo R. Tovar, tovar.nelo@gmail.com, 13-12-2012

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
