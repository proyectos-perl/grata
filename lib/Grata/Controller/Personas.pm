package Grata::Controller::Personas;
use Moose;
use Grata::Form::Persona;
use Data::Dump qw(dump);
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

#extends 'Catalyst::Controller::FormBuilder';

has 'formulario' => (isa     => 'Grata::Form::Persona',
                     is      => 'rw',
                     lazy    => 1,
                     default => sub { Grata::Form::Persona->new });

=head1 NAME

Grata::Controller::Personas - Controlador de Personas

=head1 DESCRIPTION

Controlador de Personas.

=head1 METHODS

=cut

=head2 index

=cut

sub index : Path : Args(0) {
    my ($self, $c) = @_;

    $c->stash(template => 'personas/index.tt2');
}

=head2 agregar

Agregar una Persona a la Base de Datos

=cut

sub agregar : Local {
    my ($self, $c) = @_;
    my $persona = $c->model('BD::Persona')->new_result({});

    eval {
        $self->formulario->process(item   => $persona,
                                   params => $c->req->parameters);
    };

    if ($@) {
        $c->stash->{mensaje_error} = 'La persona no pudo ser agregada';
        $c->log->error($@);
    } elsif (   $self->formulario->ran_validation
             && $self->formulario->validated) {
        $c->flash(  mensaje_estatus => 'Persona de c&eacute;dula &#171;<i>'
                  . $self->formulario->field('id')->value
                  . '</i>&#187; agregada con &eacute;xito');
        $c->response->redirect($c->uri_for('/personas'));
    }

    $c->stash(template => 'personas/agregar.tt2',
              form     => $self->formulario);

}    # ----------  end of subroutine agregar  ----------

=head2 editar

Editar una Persona a la Base de Datos

=cut

sub editar : Local {
    my ($self, $c, $id) = @_;

    #$self->formulario->field('id')->html_attr({readonly => 1});
    eval {
        $self->formulario->process(item_id => $id,
                                   params  => $c->req->parameters,
                                   schema  => $c->model('BD')->schema);
    };

    unless ($self->formulario->field('id')->value) {
        $c->log->debug('id=' . $self->formulario->field('id')->value);
        $c->flash(mensaje_error =>
                "La Persona de c&eacute;dula &#171;<i>$id</i>&#187; no existe");
        $c->log->warn("Se trató de EDITAR una Persona que no existe:$id");
        $c->response->redirect($c->uri_for('/personas'));
        return;
    }

    if ($@) {
        $c->stash->{mensaje_error} = 'La persona NO PUDO ser modificada';
        $c->log->error($@);
    } elsif ($self->formulario->ran_validation && $self->formulario->validated)
    {
        $c->flash->{mensaje_estatus} =
            "Persona de c&eacute;dula &#171;<i>$id</i>&#187; "
          . "modificada con &eacute;xito";
        $c->response->redirect($c->uri_for('/personas'));
    }

    $c->stash(template => 'personas/editar.tt2', form => $self->formulario);

}    # ----------  end of subroutine editar  ----------

sub borrar : Local {
    my ( $self, $c, $id )	= @_;

    eval{$c->model('BD::Persona')->find($id)->delete;};

    if($@) {
        $c->res->status(404);
        $c->res->body('nook');
    }else{
        $c->res->status(200);
        $c->res->body('ok');
    }
} ## --- end sub borrar

=head2 persona_dataTable 

Subrutina para manejar el intercambio de datos (json) via ajax con el pluging dataTable

=cut

sub persona_dataTable : Local {
    my ($self, $c) = @_;
    my %where;
    my $orden = 'id';

     if ($c->req->param('sSearch') =~ /\d+/) {
         $where{'cast(id as text)'} = {'like', $c->req->param('sSearch') . '%'};
     } elsif ($c->req->param('sSearch') =~ /\w+/) {
         $where{-or} = [nombre   => {'like', $c->req->param('sSearch') . '%'},
                        apellido => {'like', $c->req->param('sSearch') . '%'}];
         $orden = 'nombre, apellido';
     }

    $c->stash(
        no_wrapper => 1,
        template   => 'personas/persona_dataTable.tt2',
        sEcho      => $c->req->param('sEcho'),           # Se debe pasar directo
        iTotalRecords =>
          $c->model('BD::Persona')->count,    # Cantidad total de Registros
        iTotalDisplayRecords => $c->model('BD::Persona')->search({%where})
          ->count,                            # Cantidad de Registros Filtrados
        personas => [
            $c->model('BD::Persona')->search(
                {%where},
                {page => $c->req->param('iDisplayStart')
                   || 1,                      # Página a mostrar
                 rows => $c->req->param('iDisplayLength')
                   || 10,                     # Nro de Registros a recuperar
                 order_by => $orden})]);
}

=head1 AUTHOR

Nelo R. Tovar, tovar.nelo@gmail.com, 25/10/2011

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
