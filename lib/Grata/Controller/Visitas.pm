package Grata::Controller::Visitas;
use Moose;
use Grata::Form::Visitas;
use Grata::Form::Visitas::Buscar;
use namespace::autoclean;
use Data::Dumper;
use POSIX qw/ strftime /;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

Grata::Controller::Visitas - Controlador de Visitas

=head1 DESCRIPTION

Controlador de Visitas.

=head1 METHODS

=cut

=head2 index

=cut

sub index : Path : Args(0) {
	my ($self, $c) = @_;
	my $buscar = Grata::Form::Visitas::Buscar->new;

	$buscar->process(params => $c->req->parameters);
	if ($buscar->validated) {
		$c->go('agregar', [], [$buscar->field('persona')->value]);
	} else {
		$c->stash(form => $buscar, template => 'visitas/index.tt2');
	}
}

=head2 agregar

Agregar un Visitas a la Base de Datos

=cut

sub agregar : Local {
	my ($self, $c, $id_visitante) = @_;
	my $formulario = Grata::Form::Visitas->new;
	my $foto_vte;

	$formulario->action($c->uri_for('/visitas/agregar/'));
	$formulario->field('cargo')
	  ->options(
		cargar_opciones($c->model('BD::TipoVisitante'), 'id', 'denominacion')
	  );
	$formulario->field('visitado')->options(cargar_opciones_personal($c));
	$formulario->field('motivo')
	  ->options(
		cargar_opciones($c->model('BD::Motivo'), 'id', 'denominacion'));
	$formulario->field('zona')
	  ->options(cargar_opciones($c->model('BD::Zona'), 'id', 'denominacion'));

	if ($id_visitante) {

		$formulario->field('persona')->value($id_visitante);
		$formulario->field('persona')->readonly(1);

		my $persona = $c->model('BD::Persona')->find($id_visitante);
		if ($persona) {
			$formulario->field('nombre')->value($persona->nombre);
			$formulario->field('apellido')->value($persona->apellido);
			$formulario->field('sexo')->value($persona->sexo);
			$formulario->field('edo_civil')->value($persona->edo_civil);
			$formulario->field('telefono')->value($persona->telefono);
			$formulario->field('correo_pers')->value($persona->correo_pers);
			$formulario->field('cargo')->value($persona->tipo_visitante->id);
			$foto_vte = '/fotos/' . $persona->foto;
		}
	} else {
		my @parametros;
		foreach ($c->req->param) {
			push(@parametros, $_ => $c->req->param($_)) if ($_ ne 'foto');
		}
		push(@parametros, foto => $c->req->upload('foto'));
		$formulario->process(params => {@parametros});

		if ($formulario->validated) {
			eval {
				my $scope_guard =
				  $c->model('BD')->txn_scope_guard;    #crear transacción

				my $procedencia;
				if ($formulario->field('procedencia')->value =~ /^(\d+)-\w+/)
				{

					# Buscar la procedencia cuyo id esta en el campo del
					# formulario
					$procedencia = $c->model('BD::Procedencia')->find($1);
				} else {

					# Agregar la nueva procedencia
					$procedencia = $c->model('BD::Procedencia')->create(
						{   denominacion =>
							  $formulario->field('procedencia')->value
						}
					);
				}

				# Colocar la denominacion de la procedencia en el formulario
				$formulario->field('procedencia')
				  ->value($procedencia->denominacion);

				my $persona = $c->model('BD::Persona')->update_or_create(
					{   id        => $formulario->field('persona')->value,
						nombre    => $formulario->field('nombre')->value,
						apellido  => $formulario->field('apellido')->value,
						sexo      => $formulario->field('sexo')->value,
						edo_civil => $formulario->field('edo_civil')->value,
						telefono  => $formulario->field('telefono')->value,
						correo_pers =>
						  $formulario->field('correo_pers')->value,
						tipo_visitante => $formulario->field('cargo')->value
					}
				);
				my $visitas = $c->model('BD::Visita')->create(
					{   visitante   => $formulario->field('persona')->value,
						visitado    => $formulario->field('visitado')->value,
						motivo      => $formulario->field('motivo')->value,
						zona        => $formulario->field('zona')->value,
						procedencia => $procedencia->id,
						tipo_visitante => $formulario->field('cargo')->value,
						nro_carnet => $formulario->field('nro_carnet')->value,
						observaciones => $formulario->field('observaciones')->value,
						fecha_visita   => \'current_date',
						hora_visita    => \'current_time',
					    usuario => $c->user->id,
					}
				);
				$scope_guard->commit;

				actualizar_foto($c, $formulario->field('persona')->value);
			};
		}

		if ($@) {
			$c->stash->{mensaje_error} = 'La Visita no pudo ser agregada';
			$c->log->error('No se agrego la Visita: ' . $@);
		} elsif ($formulario->ran_validation
			&& $formulario->validated)
		{
			$c->flash(mensaje_estatus => 'Visita agregada con &eacute;xito');
			$c->response->redirect($c->uri_for('/visitas'));
		}
	}

	$c->stash(
		template => 'visitas/agregar.tt2',
		form     => $formulario,
		foto     => $foto_vte,
	);

}    # ----------  end of subroutine agregar  ----------

=head2

salida Registra la salida de una Visita

=cut

sub salida : Local {
	my ($self, $c, $id_visita) = @_;
	my $visita = $c->model('BD::Visita')->find($id_visita);

	if ($visita) {
		if (defined($visita->hora_salida)) {
			$c->stash->{mensaje_error} = "Salida ya registrada $id_visita";
		} else {
			$visita->update({hora_salida => '\now'});
		}
	} else {
		$c->stash->{mensaje_error} = "La visita no existe $id_visita";
	}
	$c->forward('index');

}    # ----------  end of subroutine salida  ----------

=head2

info Muestra los datos de una Visita

=cut

sub info : Local {
	my ($self, $c, $id_visita) = @_;
	my $visita = $c->model('BD::Visita')->find($id_visita);

	$c->stash->{foto} =
	  ($visita->visitante->foto ? '/fotos/' . $visita->visitante->foto : '');
	$c->stash->{visita}   = $visita;
	$c->stash->{template} = 'visitas/mostrar_visita.tt2';
}    # ----------  end of subroutine info_visita  ----------

sub actualizar_foto : Private {
	my ($c, $id_visitante) = @_;

	if (my $upload = $c->req->upload('foto')) {
		my $dir_destino = $c->config->{home} . '/root/fotos/';
		my $origen      = $upload->filename;
		my $nombre_foto =
		  "$id_visitante\_" . strftime("%Y%m%d_%H%M", localtime) . ".png";
		my $destino = $dir_destino . $nombre_foto;

		if ($upload->link_to($destino) || $upload->copy_to($destino)) {

			# Grabar el nombre del archivo con la foto en el registro del
			# visitante
			eval {
				$c->model('BD::Persona')->find($id_visitante)
				  ->update({foto => $nombre_foto});
			};
			if ($@) {
				$c->log->error($@);
				$c->stash->{mensaje_error} =
				  "Error al asociar la foto del vistante $id_visitante";
			}
		} else {
			$c->log->debug("Falla al subir la foto $origen a $destino : $!");
			$c->stash->{mensaje_error} = 'Error al subir la foto';
		}
	}
}

sub cargar_dataTable : Local {
	my ($self, $c) = @_;
	my %where;
	my $orden = 'me.id desc';

	$where{'visitante.nombre'} =
	  {'like', '%' . $c->req->param('sSearch') . '%'};
	$where{fecha_visita} = {'=' => \'current_date'};

	$c->stash(
		no_wrapper => 1,
		template   => 'visitas/cargar_dataTable.tt2',
		sEcho      => $c->req->param('sEcho')||1,         # Se debe pasar directo
		iTotalRecords =>
		  $c->model('BD::Visita')->count,    # Cantidad total de Registros
		iTotalDisplayRecords =>
		  $c->model('BD::Visita')->search({%where}, {prefetch => 'visitante'})
		  ->count,                           # Cantidad de Registros Filtrados
		visitas => [
			$c->model('BD::Visita')->search(
				{%where},
				{   prefetch => [qw /visitante visitado/],
					offset   => $c->req->param('iDisplayStart')
					  || 0,    # Registro inicial
					rows => $c->req->param('iDisplayLength')
					  || 10,    # Nro de Registros a recuperar
					order_by => $orden
				}
			)
		]
	);
}

sub autoc_pers : Local {
	my ($self, $c) = @_;
	my $orden = 'me.persona';

	$c->stash(
		no_wrapper => 1,
		template   => 'visitas/autocomplete_pers.tt2',
		personas   => [
			$c->model('BD::Personal')->search(
				{   'me.persona::text' =>
					  {'like', $c->req->param('term') . '%'}
				},
				{join => 'persona', order_by => $orden}
			)
		]
	);
}

sub autoc : Local {
	my ($self, $c, $id_tabla) = @_;
	my %tablas = (
		procedencia => 'BD::Procedencia',
		cargo       => 'BD::Cargo',
		zona        => 'BD::Zona'
	);

	$c->stash(
		no_wrapper => 1,
		template   => 'visitas/autocomplete.tt2',
		datos      => [
			$c->model($tablas{$id_tabla})->search(
				{   'upper(me.denominacion)' =>
					  {'like', '%' . uc($c->req->param('term')) . '%'}
				},
				{order_by => 'denominacion'}
			)
		]
	);
}

sub cargar_opciones {
	my ($rs, $campo_value, $campo_label) = @_;

	return [
		map {
			{   value => $_->$campo_value,
				label => $_->get_column($campo_label)
			}
		  } $rs->search({estatus => 'ACT'}, {order_by => $campo_label})
	];
}

sub cargar_opciones_personal {
	my ($c) = @_;
	my $rs = $c->model('BD::Personal');

	return [
		map {
			{value => $_->persona->id, label => $_->persona->nombre_completo}
		  } $rs->search(
			{'me.estatus' => 'ACT'},
			{   prefetch => [qw/persona/],
				order_by => 'persona.nombre, persona.apellido'
			}
		  )
	];
}

=head1 AUTHOR

Nelo R. Tovar, tovar.nelo@gmail.com, 02/10/2012.

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
