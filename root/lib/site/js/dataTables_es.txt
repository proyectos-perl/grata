{
	"sProcessing":   "Procesando...",
	"sLengthMenu":   "Mostrar _MENU_ registros por página",
	"sZeroRecords":  "No hay información",
	"sInfo":         "Mostrando del _START_ al _END_ de _TOTAL_ registros",
	"sInfoEmpty":    "Mostrando 0 al 0 de 0 registros",
	"sInfoFiltered": "(de un total de _MAX_ registros)",
	"sInfoPostFix":  "",
	"sSearch":       "Buscar:",
	"sUrl":          "",
	"oPaginate": {
		"sFirst":    "Primero",
		"sPrevious": "Anterior",
		"sNext":     "Siguiente",
		"sLast":     "Último"
	}
}
